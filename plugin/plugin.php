<?php
///////////////////////////////////////////////////////////////////////////

require_once 'lib/default_dune_plugin.php';
require_once 'lib/utils.php';


require_once 'tools/node_storage.php';

require_once 'tools/ScreenMainMenu.php';
require_once 'tools/ScreenServices.php';
require_once 'tools/ScreenNavigator.php';
require_once 'tools/ScreenArchive.php';
require_once 'tools/ScreenSettings.php';
require_once 'tools/ScreenRss.php';
require_once 'tools/ScreenInfo.php';


///////////////////////////////////////////////////////////////////////////

class Plugin extends DefaultDunePlugin
{
    private $_node_storage_map;
    private $_screen_rss;

    public function __construct()
    {
	$this->_node_storage_map = array
	(
	    'services'	=> new NodeStorage('services'),
	    'fav'	=> new NodeStorage('fav'),
	);

	$this->add_screen( new ScreenMainMenu() );

	$this->add_screen( new ScreenServices($this->_node_storage_map) );
	$this->add_screen( new ScreenArchive() );
	$this->add_screen( new ScreenSettings() );

	$this->add_screen( new ScreenNavigator($this->_node_storage_map) );
	$this->_screen_rss = new ScreenRss($this->_node_storage_map);
	$this->add_screen($this->_screen_rss);

	$this->add_screen( new ScreenInfo($this->_screen_rss, $this->_node_storage_map) );
    }

    public function get_vod_info($media_url_str, &$plugin_cookies)
    {
	hd_print("get_vod_info(): $media_url_str");

	$vod_info = $this->_screen_rss->get_vod_info4stored_file();
	if($vod_info)
	    return $vod_info;


	$media_url = MediaUrl::decode($media_url_str);
	if($media_url->__get('screen_id') === 'archive')
	    return $this->get_screen_by_id('archive')->get_vod_info($media_url, $plugin_cookies);

	$ns = $media_url->__get('ns');
	return $this->_node_storage_map[$ns]->get_vod_info($media_url, $plugin_cookies);
    }

    public function get_vod_stream_url($url, &$plugin_cookies)
    {
        $real_url = $url;
        if(ScreenSettings::need_check_real_url($plugin_cookies))
            $real_url = HD::get_real_url($real_url);

        if(preg_match("/\.mp3$/", $real_url))
            $real_url = preg_replace('/(https?:\/\/)/', '$1mp4://', $real_url);

        hd_print("get_vod_stream_url(): in=$url -> out=$real_url");
        return $real_url;
    }

}

///////////////////////////////////////////////////////////////////////////
?>

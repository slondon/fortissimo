<?php
error_reporting (E_ALL);
#phpinfo();

$base_url = "https://gitlab.com/slondon/fortissimo/raw/master/updates";
if( preg_match("/^update_info=1/", $_SERVER["argv"][0]) )
{
    $content_type = "text/xml";
    $url = "$base_url/update_info.xml";
}
else if( preg_match("/^ver=(.+)/", $_SERVER["argv"][0], $ver) )
{
    $content_type = "application/octet-stream";
    $url = "$base_url/dune_plugin_fortissimo-" . $ver[1] . ".tgz";
}

$doc = file_get_contents($url);
header('Content-Type: ' . $content_type);
header('Content-Length: ' . strlen($doc));
echo $doc;

?>
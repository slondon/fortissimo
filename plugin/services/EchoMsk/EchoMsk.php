<?php


require_once 'tools/service.php';


class EchoMskService extends Service
{
    const ID = 'EchoMsk';

    public function __construct()
    {
	parent::__construct(self::ID);
    }


    public function load_main_folder()
    {
	$items = array();

	foreach($this->_cfg['channels'] as $key => $ch)
	{
	    $node_info = new NodeInfo();
	    $node_info->id = $key;
	    $node_info->path = '';
	    $node_info->type = $ch['type'];
	    $node_info->name = $ch['title'];
	    $node_info->icon = $this->get_icon();
	    $node_info->rss_url = $ch['url'];
	    $node_info->descr = array('Title' => $node_info->name, 'Description' => $ch['descr']);

	    $func_name = 'load_channels';
	    if($ch['type'] === 'rss')
		$func_name = 'load_rss';

	    $node = new Node($node_info, $this, $func_name);
	    $items[$node_info->id] = $node;
	}

	return $items;
    }

    public function load_channels($parent_node)
    {
	$url = $parent_node->rss_url;
	hd_print("parse url=$url");
	$items = array();

	$pattern = '|\<div class\="title iblock"\>'
			. '\s*'
			. '\<h2\>'
			. '\s*'
			. '\<a href\="(/programs/.+/)"\>(.+)\</a\>'
			. '\s*'
			. '\</h2\>'
			. '\s*'
			. '\<div class\="date"\>(.*)\</div\>'
			. '.+'
			. '\<div class\="author type1 iblock"\>(.*)\</div\>'
			. '|sU';

#	$doc = HD::http_get_document($url);
	$doc = file_get_contents($url);
	preg_match_all($pattern, $doc, $out,  PREG_SET_ORDER);

	$idx = 0;
	foreach($out as $i)
	{
	    $node_info = new NodeInfo();
	    $node_info->id = $idx;
	    $node_info->path = '';
	    $node_info->type = 'rss';
	    $node_info->name = trim(html_entity_decode(strip_tags($i[2])));
	    $node_info->icon = $this->get_icon();
	    $node_info->rss_url = $this->_cfg['base_url'] . $i[1] . 'rss-audio.xml';


	    $raw = explode('<span class="about">', $i[4]);
	    $raw_img = isset($raw[0]) ? $raw[0] : '';
	    $raw_authorts = isset($raw[1]) ? $raw[1] : '';


	    if(preg_match('|\<span class\="photo"\>\s*\<img .*src\="(.+)" .*/\>\s*\</span\>|sU', $raw_img, $img_url))
	    {
		$node_info->icon = $this->_cfg['base_url'] . $img_url[1];
	    }

	    $authors_str = '';
	    if(preg_match('|\<strong class\="name">(.+)\</strong\>|sU', $raw_authorts, $author))
	    {
		$authors_str = trim(strip_tags(html_entity_decode($author[1])));
	    }
	    else if(preg_match_all('|\<a href\=".+"\>(.*)\</a\>|sU', $raw_authorts, $authors, PREG_SET_ORDER))
	    {
		foreach($authors as $key => $a)
		{
		    if($key !== 0)
			$authors_str .= ', ';
		    $authors_str .= trim(strip_tags(html_entity_decode($a[1])));
		}
	    }

	    $date = preg_replace('/\s+/', ' ', trim(strip_tags($i[3])));
	    $node_info->descr = array('Title' => $node_info->name, 'Presenters' => $authors_str, 'Date & time' => $date);


	    $node = new Node($node_info, $this, 'load_rss');
	    $items[$idx] = $node;
	    $idx++;

	}

	return $items;
    }
}

?>

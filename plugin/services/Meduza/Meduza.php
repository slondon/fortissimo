<?php


require_once 'tools/service.php';


class MeduzaService extends Service
{
	const ID = 'Meduza';

	public function __construct()
	{
		parent::__construct(self::ID);
	}


	public function load_main_folder()
	{
		return $this->_load_channels();
	}


	private function _load_channels()
	{
		$url = $this->_cfg['channels_url'];
		hd_print("parse url=$url");
		$doc = file_get_contents($url);
		$json = gzinflate(substr($doc, 10, -8));
		$data = json_decode($json, true);

		$idx = 0;
		$items = array();
		foreach($data['documents']['rotation/spisok-podkastov-meduzy']['podcasts'] as $item)
		{
			$node_info = new NodeInfo();
			$node_info->id = $idx;
			$node_info->path = $idx;
			$node_info->type = 'rss';
			$node_info->name = $item['title'];
			$node_info->icon = 'https://meduza.io/' . $item['image']['base_url'];
			$node_info->descr = array('Title' => $item['title'], 'Count' => $item['episodes_count']);
			$node_info->rss_url = 'https://meduza.io/rss/' . $item['url'];


			$node = new Node($node_info, $this, 'load_rss');
			$items[$idx] = $node;
			$idx++;
		}
		return $items;
	}
}

?>


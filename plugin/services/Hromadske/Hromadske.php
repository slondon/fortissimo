<?php


require_once 'tools/service.php';


class HromadskeService extends Service
{
    const ID = 'Hromadske';

    public function __construct()
    {
	parent::__construct(self::ID);
    }


    public function load_main_folder()
    {
	return $this->_do_load_channels($this->_cfg['channels']);
    }

    private function _do_load_channels($channels)
    {
	foreach($channels as $k => $ch)
	{
	    $node_info = new NodeInfo();
	    $node_info->id = $k;
	    $node_info->path = '';
	    $node_info->type = 'rss';
	    $node_info->name = $ch['name'];
	    $node_info->icon = $this->get_icon();
	    $node_info->rss_url = $ch['src_url'];

	    $node_info->descr = array('Title' => $node_info->name);


	    $node = new Node($node_info, $this, 'load_rss');
	    $items[$k] = $node;
	}
	return $items;
    }
}

?>

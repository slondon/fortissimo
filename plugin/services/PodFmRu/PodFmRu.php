<?php


require_once 'tools/service.php';


class PodFmRuService extends Service
{
    const ID = 'PodFmRu';

    public function __construct()
    {
	parent::__construct(self::ID);
    }


    public function load_main_folder()
    {
	$items = array();

	foreach($this->_cfg['channels'] as $k => $ch)
	{
	    $load_func_name = 'load_rss';
	    if($ch['type'] === 'dir')
		$load_func_name = 'load_' . $k;

	    $node_info = new NodeInfo();
	    $node_info->id = $k;
	    $node_info->path = '';
	    $node_info->type = $ch['type'];
	    $node_info->name = $ch['title'];
	    $node_info->icon = isset($ch['icon']) ? $ch['icon'] : $this->get_icon();
	    $node_info->rss_url = $ch['url'];
	    $node_info->descr = array('Title' => $node_info->name, 'Description' => $ch['descr']);

	    $node = new Node($node_info, $this, $load_func_name);
	    $items[$node_info->id] = $node;
	}
	return $items;
    }




    public function load_categories($parent_node, $from_ndx)
    {
	$url = $parent_node->rss_url;
	hd_print("parse url=$url");
	$items = array();


	$pattern_main_menu = '|\<ul class\="main_menu"\>'
			. '\s*'
			. '(.+)'
			. '\s*'
			. '\<ul\>'
			. '\s*'
			. '\<li class\="more_in"\>.+\</li\>'
			. '\s*'
			. '(.+)'
			. '\s*'
			. '\</ul\>'
			. '.+'
			. '\</ul\>'
			. '|sU';
	$raw = file_get_contents($url);
	preg_match($pattern_main_menu, $raw, $out);
	$cats_raw = $out[1];
	$cats_raw_more = $out[2];

	$pattern_items = '|\<li\>'
			. '\s*'
			. '\<a href\="(http\://.+)".*\>'
			. '.+'
			. '\<span class\="namehref2"\>(.+)\</span\>'
			. '\s*'
			. '\</a\>'
			. '\s*'
			. '\</li\>'
			. '|sU';
	preg_match_all($pattern_items, $cats_raw, $out1,  PREG_SET_ORDER);

	$pattern_items_more = '|\<li\>'
			. '\s*'
			. '\<a href\="(http\://.+)".*\>(.+)\</a\>'
			. '\s*'
			. '\</li\>'
			. '|sU';
	preg_match_all($pattern_items_more, $cats_raw_more, $out2,  PREG_SET_ORDER);

	$out = array_merge($out1, $out2);

	$idx = 0;
	foreach($out as $i)
	{
	    $node_info = new NodeInfo();
	    $node_info->id = $idx;
	    $node_info->path = '';
	    $node_info->type = 'dir';
	    $node_info->name = trim(html_entity_decode(strip_tags($i[2])));
	    $node_info->icon = $this->get_icon();
	    $node_info->rss_url = $i[1];
	    $node_info->descr = array($node_info->name);

	    $idx++;
	    $node = new Node($node_info, $this, 'load_category_items');
	    $items[$node_info->id] = $node;
	}
	return $items;
    }




    public function load_programmes($parent_node, $from_ndx)
    {
	$url = $parent_node->rss_url;
	hd_print("parse url=$url");

	$rss_node = NULL;
	try
	{
	    $rss_node = new SimpleXMLElement($url, LIBXML_COMPACT, true);
	}
	catch (Exception $e)
	{
	    hd_print("xml exception");
	}
	if(!$rss_node)
	{
	    hd_print("xml error: in SimpleXMLElement()");
	    return array();
	}

	$rss_node_ch = $rss_node->{"channel"};
	if(!$rss_node_ch)
	{
	    hd_print("xml error: can't find tag 'channel'");
	    return array();
	}

	$idx = 0;
	foreach($rss_node_ch->children() as $n)
	{
	    if($n->getName() !== 'item') continue;

	    $descr = $n->{"description"};
	    preg_match('|\<img src\="(http\://.+)" align\=.+\>|', $descr, $out);
	    $icon_url = $out[1];
	    $descr = trim(html_entity_decode(strip_tags($descr)));

	    $node_info = new NodeInfo();
	    $node_info->id = $idx;
	    $node_info->path = '';
	    $node_info->type = 'rss';
	    $node_info->name = trim(html_entity_decode(strip_tags($n->{"title"})));
	    $node_info->icon = $icon_url;

	    $ns_podfm = $n->children('podfm', true);
	    if($ns_podfm)
		$node_info->rss_url = $ns_podfm->{"linkrss"};
	    else
		$node_info->rss_url = $n->{"link"} . 'rss/rss.xml';

	    $node_info->descr = array
	    (
		'Title'		=> $node_info->name,
		'Last issue'	=> $n->{"pubDate"},
		'Description'	=> $descr,
	    );

	    $idx++;
	    $node = new Node($node_info, $this, 'load_rss');
	    $items[$node_info->id] = $node;
	}
	return $items;
    }


    public function load_category_items($parent_node, $from_ndx)
    {
	$url = $parent_node->rss_url;
	hd_print("parse url=$url");
	$items = array();

	$pattern_items = '|\<li class\="slide"\>'
			. '\s*'
			. '\<div class\="left"\>\<a href\="(http\://.+)"\>\<img src\=(http\://.+) .*\>\</a\>\</div\>'
			. '\s*'
			. '\<div class\="right"\>\<a href\=".+" class\=".+"\>(.+)\</a\>\<br /\>'
			. '\s*'
			. '\<span\>(.*)\</span\>'
			. '.+'
			. '\</li\>'
			. '|sU';

#	$doc = HD::http_get_document($url);
	$raw = file_get_contents($url);
	preg_match_all($pattern_items, $raw, $out,  PREG_SET_ORDER);


	$idx = 0;
	$node_info = new NodeInfo();
	$node_info->id = $idx;
	$node_info->path = '';
	$node_info->type = 'rss';
	$node_info->name = $parent_node->name . ' — общая лента всех передач';
	$node_info->icon = $this->get_icon();
	$node_info->rss_url = $parent_node->rss_url . '/rss/rss.xml';
	$node_info->descr = array($node_info->name);

	$node = new Node($node_info, $this, 'load_rss');
	$items[$node_info->id] = $node;
	$idx++;




	foreach($out as $i)
	{
	    $node_info = new NodeInfo();
	    $node_info->id = $idx;
	    $node_info->path = '';
	    $node_info->type = 'rss';
	    $node_info->name = trim(html_entity_decode(strip_tags($i[3])));
	    $node_info->icon = $this->_get_prog_icon($i[1]);
	    $node_info->rss_url = $i[1] . '/rss/rss.xml';
	    $node_info->descr = array
	    (
		'Title'		=> $node_info->name,
		'Description'	=> trim(html_entity_decode(strip_tags($i[4]))),
	    );

	    $idx++;
	    $node = new Node($node_info, $this, 'load_rss');
	    $items[$node_info->id] = $node;
	}
	return $items;
    }


    public function load_authors($parent_node, $from_ndx)
    {
	$items = array();
	$urls_str = $parent_node->rss_url;
	$idx = 0;
	foreach(explode(" ", $urls_str) as $url)
	{

	    hd_print("parse url=$url");
	    $raw = file_get_contents($url);
	    $pattern = '|\<div class\="profilemod my"\>'
			. '.+'
			. '\<div class\="left"\>'
			. '.+'
			. '\<img src\=(.+) .*\>'
			. '.+'
			. '\<a href\=".+"\>\<h3\>(.+)\</h3\>\</a\>'
			. '.+'
			. '\<li\>\<a href\=".+\?mode\=lents"\>.+\<span\>(.+)\</span\>\</a\>\</li\>'
			. '|sU';
	    preg_match($pattern, $raw, $out);



	    $node_info = new NodeInfo();
	    $node_info->id = $idx;
	    $node_info->path = '';
	    $node_info->type = 'dir';
	    $node_info->name = $out[2];
	    $node_info->icon = $out[1];
	    $node_info->rss_url = $url;
	    $node_info->descr = array
	    (
		'Title'		=> $node_info->name,
		'Rss feeds'	=> $out[3],
	    );

	    $node = new Node($node_info, $this, 'load_author');
	    $items[$node_info->id] = $node;
	    $idx++;
	}

	return $items;
    }


    public function load_author($parent_node, $from_ndx)
    {
	$idx = 0;
	$items = array();

	$url = $parent_node->rss_url;


	$node_info = new NodeInfo();
	$node_info->id = $idx;
	$node_info->path = '';
	$node_info->type = 'rss';
	$node_info->name = $parent_node->name . ' — общая лента всех передач';
	$node_info->icon = $parent_node->icon;
	$node_info->rss_url = $url . '/rss/rss.xml';
	$node_info->descr = array($node_info->name);

	$node = new Node($node_info, $this, 'load_rss');
	$items[$node_info->id] = $node;
	$idx++;


	$url .= '?mode=lents';
	hd_print("parse url=$url");
	$raw = file_get_contents($url);
	$pattern_items = '|\<div class\="news" id=.+\>'
			. '\s*'
			. '\<div class\="left"\>\<a href\="(.+)"\>\<img src\="(.+)" .*\>\</a\>\</div\>'		// $out[1]=url, $out[2]=icon
			. '.+'
			. '\<div class\="title"\>\<a href\=".+"\>(.+)\</a\>\</div\>'				// $out[3]=title
			. '.*'
			. '\<a href\=".+" class\="authorlink"\>(.+)\</a\>\</p\>'				// $out[4]=count
			. '\s*'
			. '\<p class\=".+"\>Последняя запись: (.*)\</p\>'					// $out[5]=last
			. '.+'
			. '\<div class\="making" style\=".+"\>.+\</div\>'
			. '\s*'
			. '\<p\>(.*)\</p\>'									// $out[6]=descr
			. '|sU';
	preg_match_all($pattern_items, $raw, $out,  PREG_SET_ORDER);

	foreach($out as $i)
	{
	    $node_info = new NodeInfo();
	    $node_info->id = $idx;
	    $node_info->path = '';
	    $node_info->type = 'rss';
	    $node_info->name = trim(html_entity_decode(strip_tags($i[3])));
	    $node_info->icon = $i[2];
	    $node_info->rss_url = $i[1] . '/rss/rss.xml';
	    $node_info->descr = array
	    (
		'Title'		=> $node_info->name,
		'Count'		=> $i[4],
		'Last issue'	=> $i[5],
		'Description'	=> trim(html_entity_decode(strip_tags($i[6])))
	    );

	    $idx++;
	    $node = new Node($node_info, $this, 'load_rss');
	    $items[$node_info->id] = $node;
	}
	return $items;













	foreach(explode(" ", $urls_str) as $url)
	{

	    hd_print("parse url=$url");
	    $raw = file_get_contents($url);
	    $pattern = '|\<div class\="profilemod my"\>'
			. '.+'
			. '\<div class\="left"\>'
			. '.+'
			. '\<img src\=(.+) .*\>'
			. '.+'
			. '\<a href\=".+"\>\<h3\>(.+)\</h3\>\</a\>'
			. '.+'
			. '\<li\>\<a href\=".+\?mode\=lents"\>.+\<span\>(.+)\</span\>\</a\>\</li\>'
			. '|sU';
	    preg_match($pattern, $raw, $out);



	    $node_info = new NodeInfo();
	    $node_info->id = $idx;
	    $node_info->path = '';
	    $node_info->type = 'dir';
	    $node_info->name = $out[2];
	    $node_info->icon = $out[1];
	    $node_info->rss_url = $url;
	    $node_info->descr = array
	    (
		'Title'		=> $node_info->name,
		'Rss feeds'	=> $out[3],
	    );

	    $node = new Node($node_info, $this, 'load_author');
	    $items[$node_info->id] = $node;
	    $idx++;
	}

	return $items;
    }



#####################################

    private function _get_prog_icon($url)
    {
	$raw = file_get_contents($url);
	if(preg_match('|\<div id\="slider" class\="slpodcast"\s*\>\s*\<a href\="http\://.+"\>\<img src\=(http\://.+) .*/\>\</a\>|U', $raw, $out))
	{
	    return $out[1];
	}
	return '';
    }


}

?>

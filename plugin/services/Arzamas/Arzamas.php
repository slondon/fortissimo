<?php


require_once 'tools/service.php';


class ArzamasService extends Service
{
    const ID = 'Arzamas';

    public function __construct()
    {
	parent::__construct(self::ID);
    }


    public function load_main_folder()
    {
	$items = array();

	$node_info = new NodeInfo();
	$node_info->id = "courses";
	$node_info->path = '';
	$node_info->type = 'rss';
	$node_info->name = 'Курсы. Лекции';
	$node_info->icon = $this->get_icon();
	$node_info->rss_url = $this->_cfg['rss_url'];
	$node_info->descr = array('Title' => $node_info->name, 'Description' => $this->_cfg['description']);
	$node = new Node($node_info, $this, 'load_rss');
	$items[$node_info->id] = $node;

	return $items;
    }
}

?>

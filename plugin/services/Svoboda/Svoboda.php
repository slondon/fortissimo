<?php


require_once 'tools/service.php';


class SvobodaService extends Service
{
    const ID = 'Svoboda';

    public function __construct()
    {
	parent::__construct(self::ID);
    }


    public function load_main_folder()
    {
	$depts = array();
	foreach($this->_cfg['departments'] as $id => $d)
	{
	    $node_info = new NodeInfo();
	    $node_info->id = $id;
	    $node_info->path = '';
	    $node_info->type = 'dir';
	    $node_info->name = $d['name'];
	    $node_info->rss_url = '';
	    $node_info->icon = $this->get_icon();
	    $node_info->descr = array('Title'=>$d['name'], 'Description'=>$d['descr']);
	    $node = new Node($node_info, $this, 'load_channels');
	    $depts[$id] = $node;

	}
	return $depts;
    }


    public function load_channels($parent_node)
    {
	$id = $parent_node->id;
	$url = $this->_cfg['departments'][$id]['src_url'];

	hd_print("parse url=$url");
	$items = array();
	$pattern = '|\<div class\="media-block "\>'
			. '.*'
			. '\<img data-src\="(.*)".*\>'																# 1 - icon
			. '.*'
			. '\<h4 class\=".+" title\="(.+)"\>.+\</h4\>'												# 2 - title
			. '\s*'
			. '\<p class="perex perex--mb perex--size-2".*\>(.+)\</p\>\s*\</a\>'						# 3 - descr
			. '.*'
			. '\<a class\="podcast-sub__modal-link podcast-sub__modal-link--rss" href\="(.+)".*\>'		# 4 - url
			. '|sU';
    
#	$doc = HD::http_get_document($url);
	$doc = file_get_contents($url);
	preg_match('|\<div class\="tab-content" id\="rssItems"\>(.+)|s', $doc, $tmp);
	$tmp = str_replace(array ('&#171;', '&#187;'), '', $tmp);
	preg_match_all($pattern, $tmp[1], $out, PREG_SET_ORDER);

	$url_scheme = parse_url($url, PHP_URL_SCHEME);
	$url_hostname = parse_url($url, PHP_URL_HOST);

	$idx = 0;
	foreach($out as $i)
	{
		$node_info = new NodeInfo();
		$node_info->id = $idx;
		$node_info->path = $idx;
		$node_info->type = 'rss';
		$node_info->name = trim(html_entity_decode(strip_tags($i[2])));
		$node_info->icon = $i[1];

		$d = trim(str_replace('&nbsp;', ' ', strip_tags($i[3])));
		$node_info->descr = array('Title' => $node_info->name, 'Description' => $d);

		$url_tmp = preg_replace('/\?count=20&/', '?count=100&', htmlspecialchars_decode($i[4]));
		$pos = strpos($url_tmp, 'http');
		if($pos === false || $pos != 0)
		{
			$url_tmp = $url_scheme . '://' . $url_hostname . '/' . $url_tmp;
		}
		$node_info->rss_url = $url_tmp;


		$node = new Node($node_info, $this, 'load_rss');
		$items[$idx] = $node;
		$idx++;

	}

	return $items;
    }
}

?>

<?php


require_once 'tools/service.php';


class KinopoiskService extends Service
{
    const ID = 'Kinopoisk';

    public function __construct()
    {
	parent::__construct(self::ID);
    }


    public function load_main_folder()
    {
	$depts = array();
	foreach($this->_cfg['sources'] as $id => $s)
	{
	    $node_info = new NodeInfo();
	    $node_info->id = $id;
	    $node_info->path = '';
	    $node_info->type = 'rss';
	    $node_info->name = $s['name'];
	    $node_info->rss_url = $s['rss_feed'];
	    $node_info->icon = $s['icon_url'];
	    $node_info->descr = array('Title'=>$s['name'], 'Description'=>$s['descr']);
	    $node = new Node($node_info, $this, 'load_rss');
	    $srcs[$id] = $node;

	}
	return $srcs;
    }
}

?>

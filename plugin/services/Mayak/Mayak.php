<?php


require_once 'tools/service.php';


class MayakService extends Service
{
    const ID = 'Mayak';

    public function __construct()
    {
        parent::__construct(self::ID);
    }


    public function load_main_folder()
    {
        return $this->_do_load_categories();
    }

    public function load_category($parent_node)
    {
        $url = $parent_node->rss_url;
        $title = $parent_node->title;

        hd_print("load category: title='$title', url=$url");
        $doc = file_get_contents($url);

        $pattern = '|\<div class\="podcast-list__item"\>'
            . '\s*'
            . '\<div class\="podcast-list__preview"\>'
            . '\s*'
            . '\<div class\="podcast-list__picture"\>'
            . '\s*'
            . '\<a class\="podcast-list__link"'
            . '\s+'
            . 'href\="(.+)"\>'                                  # i[1]=url
            . '\s*'
            . '\<img src\="(.+)".+title\="(.+)"\>'              # i[2]=icon; i[3]=title
            . '.+'
            . '\<div class\="podcast-list__anons"\>'
            . '\s*'
            . '\<a class\="podcast-list__link"'
            . '\s+'
            . 'href\=".+"\>(.+)\<div'                           # i[4]=descr
            . '.+'
            . '\<div class\="podcast-list__count"\>'
            . '\s*'
            . '\<a class\="podcast-list__link"'
            . '\s+'
            . 'href\=".+"\>(.+)\</a\>'                          # i[5]=count
            . '|sU';


        $doc = file_get_contents($url);
        preg_match_all($pattern, $doc, $out,  PREG_SET_ORDER);

        $idx = 0;
        $items = array();
        foreach($out as $i)
        {
            $node_info = new NodeInfo();
            $node_info->id = $idx;
            $node_info->path = '';
            $node_info->type = 'dir';

            $title = trim(html_entity_decode(strip_tags($i[3])));
            $node_info->name = $title;

            $node_info->icon = $i[2];
            $node_info->rss_url = $this->_cfg['base_url'] . $i[1];
            $node_info->descr = array
            (
                'Title'         => $title,
                'Count'         => trim(html_entity_decode(strip_tags($i[5]))),
                'Description'   => trim(html_entity_decode(strip_tags($i[4]))),
            );

            $node = new Node($node_info, $this, 'load_channel_media');
            $items[$idx] = $node;
            $idx++;
        }
        return $items;
    }


    public function load_channel_media($parent_node)
    {
        $items = array();
        $url = $parent_node->rss_url;

        hd_print("load_channel_media url=$url");
        $doc = file_get_contents($url);

        $pattern = '|(.+)'                                                                          # out[1]=*<span class="podcast-header__{completed|updating}">*
                    . '\<a class\="podcast-header__link podcast-header__link--rss no-ajaxy"'
                    . '\s+'
                    . 'href\="(.+)"\>.+\</a\>'                                                      # out[2]=url
                    . '|sU';
        if(preg_match($pattern, $doc, $out))
        {
            $rss_url = $this->_cfg['base_url'] . $out[2];
            hd_print("get audio rss url: $url -> $rss_url");
            $node_info = new NodeInfo();
            $node_info->id = 'audio';
            $node_info->path = '';
            $node_info->name = $parent_node->name . ' [Audio]';
            $node_info->icon = $parent_node->icon;
            $node_info->descr = $parent_node->descr;
            $node_info->descr['Status'] = 'Обновляется';
            if(preg_match('|\<span class\="podcast-header__completed"\>|sU', $out[1]))
            {
                $node_info->descr['Status'] = 'Завершен';
            }
            $node_info->type = 'rss';
            $node_info->rss_url = $rss_url;

            $node = new Node($node_info, $this, 'load_rss');
            $items[$node_info->id] = $node;
        }
        return $items;
    }


    private function _do_load_categories()
    {
        $url = $this->_cfg['categories_url'];
        hd_print("parse categories: url=$url");
        $categories = array();

        $pattern = '|\<li class\="podcast-categories__item.*"\>'
            . '\s*'
            . '\<div class\="podcast-categories__info"\>'
            . '\s*'
            . '\<h2 class\="podcast-categories__title"\>'
            . '\s*'
            . '\<a class\="podcast-categories__link"\s+href\="(.+)"\>(.+)\</a\>'        # i[1]=url; i[2]=title
            . '.+'
            . '\<div class\="podcast-categories__count"\>'
            . '\s*'
            . '\<a class\="podcast-categories__link"\s+href\=".+"\>(.+)\</a\>'          # i[3]=count
            . '|sU';

        $doc = file_get_contents($url);
        preg_match_all($pattern, $doc, $out,  PREG_SET_ORDER);

        $idx = 0;
        foreach($out as $i)
        {
            $node_info = new NodeInfo();
            $node_info->id = $idx;
            $node_info->path = '';
            $node_info->type = 'dir';

            $title = trim(html_entity_decode(strip_tags($i[2])));
            $node_info->name = $title;

            $node_info->icon = $this->get_icon();
            $node_info->rss_url = $this->_cfg['base_url'] . $i[1];
            $node_info->descr = array
            (
                'Title'     => $title,
                'Count'     => trim(html_entity_decode(strip_tags($i[3]))),
            );

            $node = new Node($node_info, $this, 'load_category');
            $categories[$idx] = $node;
            $idx++;
        }
        return $categories;
    }

}

?>

<?php


require_once 'tools/service.php';


class RadioVestiService extends Service
{
    const ID = 'RadioVesti';

    public function __construct()
    {
	parent::__construct(self::ID);
    }


    public function load_main_folder()
    {
	$items = array();

	$node_info = new NodeInfo();
	$node_info->id = "0";
	$node_info->path = '';
	$node_info->type = 'rss';
	$node_info->name = $this->_cfg['title'];
	$node_info->icon = $this->get_icon();
	$node_info->rss_url = $this->_cfg['rss_url'];
	$node_info->descr = array('Title' => $node_info->name, 'Description' => $this->_cfg['description']);

	$func_name = 'load_rss';

	$node = new Node($node_info, $this, 'load_rss');
	$items[$node_info->id] = $node;

	return $items;
    }
}

?>

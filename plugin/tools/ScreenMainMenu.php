<?php

require_once 'lib/abstract_regular_screen.php';

require_once 'tools/Common.php';


class ScreenMainMenu extends AbstractRegularScreen implements UserInputHandler
{
    const ID = 'main_menu';

    public function __construct()
    {
	parent::__construct(self::ID, self::get_folder_views());
	UserInputHandlerRegistry::get_instance()->register_handler($this);
    }

    public function get_handler_id()
    {
	return self::ID;
    }

    public function get_folder_range(MediaURL $media_url, $from_ndx, &$plugin_cookies)
    {
	$items = array();
	$item = array
	(
	    PluginRegularFolderItem::caption		=> 'Favorites',
	    PluginRegularFolderItem::view_item_params	=> array
	    (
		ViewItemParams::icon_path		=> 'plugin_file://icons/main_menu/Favorites_256x256.png',
		ViewItemParams::item_layout		=> HALIGN_CENTER,
		ViewItemParams::icon_valign		=> VALIGN_CENTER,
	    ),
	    PluginRegularFolderItem::media_url		=> MediaURL::encode(array('screen_id' => 'navigator', 'ns' => 'fav')),
	);
	$items[] = $item;

	$item[PluginRegularFolderItem::caption] = 'Services';
	$item[PluginRegularFolderItem::view_item_params][ViewItemParams::icon_path] = 'plugin_file://icons/main_menu/Globe_256x256.png';
	$item[PluginRegularFolderItem::media_url] = MediaURL::encode(array('screen_id' => 'services', 'ns' => 'services'));
	$items[] = $item;

	$item[PluginRegularFolderItem::caption] = 'Archive';
	$item[PluginRegularFolderItem::view_item_params][ViewItemParams::icon_path] = 'plugin_file://icons/main_menu/Archive_256x256.png';
	$item[PluginRegularFolderItem::media_url] = 'archive';
	$items[] = $item;

	$item[PluginRegularFolderItem::caption] = 'Settings';
	$item[PluginRegularFolderItem::view_item_params][ViewItemParams::icon_path] = 'plugin_file://icons/main_menu/Settings_256x256.png';
	$item[PluginRegularFolderItem::media_url] = 'settings';
	$items[] = $item;

	$count = count($items);
	return array
	(
	    PluginRegularFolderRange::total => $count,
	    PluginRegularFolderRange::more_items_available => false,
	    PluginRegularFolderRange::from_ndx => $from_ndx,
	    PluginRegularFolderRange::count => $count,
	    PluginRegularFolderRange::items => $items
	);
    }


    static function get_folder_views()
    {
	$view_0 = array
	(
	    PluginRegularFolderView::view_params => array
	    (
		ViewParams::num_cols => 4,
		ViewParams::num_rows => 1,
#		ViewParams::icon_selection_box_width => 400,
#		ViewParams::icon_selection_box_height => 400,
#		ViewParams::icon_selection_box_dy => -225,
		ViewParams::paint_details => false,
		ViewParams::paint_icon_selection_box => false,
		
	    ),
	    PluginRegularFolderView::base_view_item_params => array
	    (
		ViewItemParams::icon_valign => VALIGN_CENTER,
		ViewItemParams::icon_sel_scale_factor => 1.2,
		ViewItemParams::item_caption_dy => -160,
		ViewItemParams::item_caption_sel_dy => -100,
		ViewItemParams::icon_keep_aspect_ratio => true,
	    ),
	    PluginRegularFolderView::async_icon_loading => false,
	    PluginRegularFolderView::not_loaded_view_item_params => array(),

	);

        return array($view_0);
    }




    public function get_action_map(MediaURL $media_url, &$plugin_cookies)
    {
	$actions = array
	(
	    GUI_EVENT_KEY_ENTER => ActionFactory::invalidate_folders(array(self::ID), ActionFactory::open_folder()),
	    GUI_EVENT_KEY_C_YELLOW => Common::action_changelog($this),
	);

	$act_info_green = Common::action_info_green($this);
	if(!is_null($act_info_green))
	    $actions[GUI_EVENT_KEY_B_GREEN] = $act_info_green;

	return $actions;
    }

    public function handle_user_input(&$user_input, &$plugin_cookies)
    {
	if (isset($user_input->control_id))
	{
	    switch ($user_input->control_id)
	    {
		case 'changelog':
		    return Common::get_changelog_dialog();
		case 'info_green':
		    return Common::get_info_green_dialog();
	    }
	}
	return array
	(
	    GuiAction::handler_string_id => PLUGIN_OPEN_FOLDER_ACTION_ID,
	    GuiAction::data => array
	    (
		'media_url' => $user_input->selected_media_url
	    ),
	);
    }

}

?>


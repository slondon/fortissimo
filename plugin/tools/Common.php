<?php


require_once 'lib/control_factory.php';
require_once 'lib/action_factory.php';



class Common
{

    private static $_info_green = NULL;
    private static $_info_green_time = NULL;



    public static function action_changelog($hdl)
    {
	$changelog = UserInputHandlerRegistry::create_action($hdl, 'changelog');
	$changelog['caption'] = 'changelog';
	return $changelog;
    }


    public static function action_info_green($hdl)
    {
	if(!is_null(self::$_info_green_time) || time() - self::$_info_green_time > 7200)
	{
#	    self::$_info_green = file_get_contents('http://127.0.0.1/plugins/foRtiSSimo/info_green.txt');
	    if(self::$_info_green)
		self::$_info_green_time = time();
	}

	if(self::$_info_green)
	{
	    $text = explode("\n", self::$_info_green);
	    if(preg_match('/BUTTON: (.+)/', $text[0], $out) === 1)
	    {
		$info_act = UserInputHandlerRegistry::create_action($hdl, 'info_green');
		$info_act['caption'] = $out[1];
		return $info_act;
	    }
	}

	return NULL;
    }


    public static function get_changelog_dialog()
    {
	$defs = array();
	$file = DuneSystem::$properties['install_dir_path'] . "/changelog.txt";
	if(file_exists($file))
	{
	    $str = file_get_contents($file);
	}
	ControlFactory::add_multiline_label($defs, '', $str, 12);
	ControlFactory::add_custom_close_dialog_and_apply_button($defs, 'setup', 'Ok', 150,  null);
	return ActionFactory::show_dialog("Changelog", $defs, true, 1600);
    }

    public static function get_info_green_dialog()
    {
	$defs = array();
	foreach(explode("\n", self::$_info_green) as $key => $line)
	{
	    if($key > 0)
	    ControlFactory::add_label($defs, '', $line);
	}
	ControlFactory::add_close_dialog_button($defs, ' close ', 150);
	return ActionFactory::show_dialog('Info', $defs, true);
    }


    public static function get_icon_path($icon)
    {
	if(! $icon)
	    return 'missing://';
	return rawurldecode($icon);
    }

    public static function get_icon4type($type)
    {
	if($type === 'dir')
	    return 'gui_skin://small_icons/folder.aai';
	if($type === 'rss')
	    return 'plugin_file://icons/rss.png';
	if($type === 'audio')
	    return 'gui_skin://small_icons/audio_file.aai';
	return 'missing://';
    }

    public static function get_screen4type($type)
    {
	if($type === 'srv_list')
	    return 'service_list';
	if($type === 'dir')
	    return 'navigator';
	if($type === 'rss')
	    return 'rss';
	return 'unknown';
    }

    public static function get_descr_str($descr)
    {
	$str = '';
	foreach($descr as $k => $v)
	{
	    if(empty($v))
		continue;
	    if(is_string($k) && count($descr) > 1)
		$str .= $k . '|' . $v . '||';
	    else
		$str .= $v . '||';
	}
	return $str;
    }


    public static function get_func_name4fav_by_type($type)
    {
	if($type === 'dir')
	    return 'navigator';
	if($type === 'rss')
	    return 'load_rss';
	return 'unknown';
    }

}

?>


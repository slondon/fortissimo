<?php


require_once 'tools/node.php';
require_once 'tools/Common.php';
require_once 'tools/service.php';
require_once 'tools/favorites_ctl.php';


class NodeStorage
{
    const ITEM_CHUNK_SIZE = 20;
    private $_root_node;



    public function __construct($type)
    {
	$func = 'unknown';
	if($type === 'services')
	    $func = 'load_service_list';
	else if($type === 'fav')
	    $func = 'load_fav_list';

	$node_info = new NodeInfo();
	$node_info->id = 'root';
	$node_info->path = '';
	$node_info->type = $type;
	$node_info->name = 'Root';
	$node_info->rss_url = '';
	$node_info->icon = '';
	$node_info->descr = '';
	$this->_root_node = new Node($node_info, $this, $func);
    }

    public function get_folder_range(MediaURL $media_url, $from_ndx, &$plugin_cookies)
    {
	hd_print("get_folder_range(): request: media_url=" . $media_url->get_raw_string() . ", from_ndx=$from_ndx");

	$items = array();
	$total = 0;
	$ns = $media_url->__get('ns');
	$path = $media_url->__get('path');
	$node = $this->get_node($path, $plugin_cookies);
	$need_reload_map = $node->type === 'rss' || $ns === 'fav' && $node->id === 'root';
	$map = $node->get_map($need_reload_map, $from_ndx, $plugin_cookies);
	if(! $map)
	    $map = array();
	$total = count($map);
	$child_nodes = array_slice($map, $from_ndx, self::ITEM_CHUNK_SIZE);
	foreach($child_nodes as $n)
	{
	    $caption = $n->name;
	    if(!empty($n->pub_date))
		$caption = $n->pub_date . " — " . $n->name;

	    $item_icon = $node->type === 'services' ? Common::get_icon_path($n->icon) : Common::get_icon4type($n->type);
	    $item = array
	    (
		PluginRegularFolderItem::caption		=> $caption,
		PluginRegularFolderItem::media_url		=> $this->_get_media_url($n->type, $ns, $n->path),
		PluginRegularFolderItem::view_item_params	=> array
		(
		    ViewItemParams::icon_path			=> $item_icon,
		    ViewItemParams::item_detailed_info		=> Common::get_descr_str($n->descr),
#		    ViewItemParams::item_detailed_icon_path	=> Common::get_icon_path($n->icon),
		    ViewItemParams::item_detailed_icon_path	=> 'missing://',
		)
	    );
	    if($n->type !== 'audio' || ScreenSettings::need_item_view_detail_poster($plugin_cookies))
		$item[PluginRegularFolderItem::view_item_params][ViewItemParams::item_detailed_icon_path] = Common::get_icon_path($n->icon);

	    $items[] = $item;
	}
	$count = count($items);
	hd_print("get_folder_range(): reply: count/total=$count/$total, from_ndx=$from_ndx");

	return array
	(
	    PluginRegularFolderRange::total => $total,
	    PluginRegularFolderRange::more_items_available => $node->is_more_items_available(),
	    PluginRegularFolderRange::from_ndx => $from_ndx,
	    PluginRegularFolderRange::count => $count,
	    PluginRegularFolderRange::items => $items
	);
    }


    public function get_vod_info(MediaURL $media_url, &$plugin_cookies)
    {
	$path = $media_url->__get('path');
	$node = $this->get_node($path, $plugin_cookies);
	if(! $node)
	    throw new FortissimoException("can't find node for path=$path");
	$vod_info = $node->info;

	return array
	(
	    PluginVodInfo::name		=> $vod_info['title'],
	    PluginVodInfo::description	=> $vod_info['description'],
#	    PluginVodInfo::description	=> $vod_info['title'],
	    PluginVodInfo::poster_url	=> Common::get_icon_path($vod_info['icon']),
	    PluginVodInfo::series	=> array
	    (
		array
		(
		    PluginVodSeriesInfo::name		=> $vod_info['title'],
		    PluginVodSeriesInfo::playback_url	=> $vod_info['media_url'],
		    PluginVodSeriesInfo::playback_url_is_stream_url => false
		)
	    )
	);
    }


    public function load_service_list($root_node, $from_ndx, $plugin_cookies)
    {
	return Service::load_service_list($root_node, $from_ndx, $plugin_cookies);
    }

    public function load_fav_list($root_node, $from_ndx, $plugin_cookies)
    {
	return FavoritesCtl::load_fav_list($root_node, $from_ndx, $plugin_cookies);
    }


    public function get_node($path, &$plugin_cookies)
    {
	if(! $this->_root_node)
	    return NULL;
	if(is_null($path) || $path === '')
	    return $this->_root_node;
	$curr_node = $this->_root_node;
	$path_arr = explode('/', $path);
	foreach($path_arr as $p)
	{
	    $map = $curr_node->get_map(false, 0, $plugin_cookies);
	    if(! $map)
		return NULL;
	    $curr_node = $map[$p];
	    if(! $curr_node)
		return NULL;
	}
	return $curr_node;
    }




###########################################################

    private function _get_media_url($type, $ns, $path)
    {
	return MediaURL::encode(array('screen_id' => Common::get_screen4type($type), 'ns' => $ns, 'path' => $path));
    }


}

?>

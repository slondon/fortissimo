<?php


require_once 'tools/service_cfg.php';
require_once 'tools/node.php';
require_once 'tools/ScreenSettings.php';
require_once 'tools/helpers.php';


abstract class Service
{
    protected $_id;
    protected $_cfg;

    public function __construct($id)
    {
	$this->_id = $id;
	$this->_reload_cfg();
    }

    public function get_id()
    {
	return $this->_id;
    }
    public function get_title()
    {
	return $this->_cfg['title'];
    }

    public function get_icon()
    {
	return "plugin_file://services/" . $this->_id . "/" . $this->_cfg['icon_file'];
    }

    public function create_node()
    {
	$node_info = new NodeInfo();
	$node_info->id = $this->_id;
	$node_info->path = '';
	$node_info->type = 'dir';
	$node_info->name = $this->_cfg['title'];
	$node_info->rss_url = '';
	$node_info->icon = $this->get_icon();
	$node_info->descr = array($this->_cfg['description']);
	return new Node($node_info, $this, 'load_main_folder');
    }


    public function load_rss(Node $node, $from_ndx, &$plugin_cookies)
    {
	if(! $node || $node->type !== 'rss')
	    throw new FortissimoException("can't load rss for node: " . $node->str());

	$rss_url = $this->_get_rss_url($node->rss_url);
	$rss = RssParser::load_rss($rss_url, $this->_cfg);
	$items = array();
	$i = 0;
	foreach($rss as $item)
	{
	    if(! isset($item['item_url']))
		continue;

	    $descr1 = isset($item['item_text']) ? $item['item_text'] : " ";
	    $descr = html_entity_decode($descr1, ENT_COMPAT, 'UTF-8');

	    $icon = $this->get_icon();
	    if(isset($item['item_img']))
		$icon = $item['item_img'];
	    else if(preg_match('|\<img .*src\="(.+)" .*\>|U', $descr, $out) >= 1)
		$icon = $out[1];

	    $descr = preg_replace('|<br>|', "\n", $descr);
	    $descr = strip_tags($descr);
	    $descr = preg_replace('|[ \t]+|', ' ', $descr);
	    $descr = preg_replace('|\n+[ \t]*\n+|', "\n", $descr);
	    $descr = preg_replace('| , |', ', ', $descr);
	    $descr = trim($descr);

	    $duration = $item['item_duration'];
	    if(!strstr($duration, ':'))
		$duration = seconds2hms($duration);

	    $file_size = get_filesize_str($item['item_length']);

	    $url = $item['item_url'];
	    if(!HD::is_android())
		$url = preg_replace('/https:\/\//', 'http://', $item['item_url']);

	    $info = array
	    (
		'title'		=> trim($item['item_title']),
		'media_url' => $url,
		'icon'		=> $icon,
		'duration'	=> $duration,
		'file_size'	=> $file_size,
		'description'	=> $descr,
	    );

	    $pub_date = DateTime::createFromFormat($this->_cfg['date_fmt_in'], trim($item['item_date']));
	    $date_out_fmt = ScreenSettings::get_item_view_caption_date_format($plugin_cookies);
	    $date_out = $pub_date->format($date_out_fmt);

	    $idi = array();
	    if( strlen($info['title']) > 0 && ScreenSettings::need_item_view_detail_title($plugin_cookies))
		$idi['Title'] = $info['title'];
	    if( strlen($date_out) > 0 && ScreenSettings::need_item_view_detail_start_time($plugin_cookies))
		$idi['Start time'] = $date_out;
	    if( strlen($duration) > 0 && ScreenSettings::need_item_view_detail_duration($plugin_cookies))
		$idi['Duration'] = $duration;
	    if( strlen($file_size) > 0 && ScreenSettings::need_item_view_detail_file_size($plugin_cookies))
		$idi['File_size'] = $file_size;
	    if( strlen($descr) > 0 && ScreenSettings::need_item_view_detail_descr($plugin_cookies))
		$idi['Description'] = preg_replace('|[ \t\n]+|', ' ', $descr);


	    $node_info = new NodeInfo();
	    $node_info->id = $i;
	    $node_info->path = $i;
	    $node_info->type = 'audio';
	    $node_info->name = $info['title'];
	    $node_info->pub_date = $date_out;
	    $node_info->info = $info;
	    $node_info->icon = $icon;
	    $node_info->descr = $idi;
	    $item_node = new Node($node_info, $this, 'none');
	    $items[] = $item_node;

	    $i++;
	}
	return $items;
    }


    static public function load_service_list($root_node, $from_ndx, $plugin_cookies)
    {
	$srv_nodes = array();
	$file_list = DuneSystem::$properties['install_dir_path'] . '/services/list';
	$srv_list_arr = file($file_list, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
	if($srv_list_arr)
	    foreach($srv_list_arr as $srv_name)
	    {
		$include = implode("/", array("services", $srv_name, $srv_name . ".php"));
		require_once $include;
		$class = $srv_name . "Service";

		$srv = new $class;
		$srv_node = $srv->create_node();
		$srv_nodes[$srv_node->id] = $srv_node;
	    }
	return $srv_nodes;
    }


#####################################################

    private function _reload_cfg()
    {
	$this->_cfg = ServiceCfg::load($this->_id);
    }


    protected function _get_rss_url($url)
    {
	hd_print("get rss url: $url -> $url");
	return $url;
    }
}

?>

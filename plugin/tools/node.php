<?php


class NodeInfo
{
    public $id;
    public $path;
    public $type;
    public $name;
    public $pub_date;
    public $rss_url;
    public $icon;
    public $descr;
    public $info;

    public function __construct()
    {
	$this->id = '';
	$this->path = '';
	$this->type = '';
	$this->name = '';
	$this->pub_date = '';
	$this->rss_url = '';
	$this->icon = '';
	$this->descr = '';
	$this->info = array();
    }

    public function str()
    {
	return "id=$this->id, path=$this->path, type=$this->type, name='$this->name', "
		. "rss_url=$this->rss_url, icon=$this->icon";
    }
}


class Node extends NodeInfo
{

    private $_map;
    private $_obj4func;
    private $_reload_map_func;
    private $_more_items_available;
    private $_next_page_url;
    private $_error_msg;

    public function __construct($node_info, $obj4func, $reload_map_func)
    {
	$this->id = $node_info->id;
	$this->path = $node_info->path;
	$this->type = $node_info->type;
	$this->name = $node_info->name;
	$this->pub_date = $node_info->pub_date;
	$this->rss_url = $node_info->rss_url;
	$this->icon = $node_info->icon;
	$this->descr = $node_info->descr;
	$this->info = $node_info->info;
	$this->_map = NULL;
	$this->_obj4func = $obj4func;
	$this->_reload_map_func = $reload_map_func;
	$this->_more_items_available = false;
	$this->_next_page_url = '';
    }

    public function get_obj_name()			{ return $this->_obj4func->get_id(); }
    public function get_obj_title()			{ return $this->_obj4func->get_title(); }
    public function str()				{ return parent::str(); }
    public function is_more_items_available()		{ return $this->_more_items_available; }
    public function set_more_items_available($more)	{ $this->_more_items_available = $more; }
    public function get_next_page_url()			{ return $this->_next_page_url; }
    public function set_next_page_url($url)		{ $this->_next_page_url = $url; }


    public function get_map($need_reload, $from_ndx, &$plugin_cookies)
    {
	$nr_int = (int)$need_reload;
	$mia_int = (int)$this->is_more_items_available();
	hd_print("get_map(): func=$this->_reload_map_func(need_reload=$nr_int, from_ndx=$from_ndx), more_items_available=$mia_int");

	if(!$this->_map || $need_reload && !$from_ndx)
	{
	    $this->_map = $this->_get_new_items($from_ndx, &$plugin_cookies);
	}
	else if($this->is_more_items_available() && $from_ndx === count($this->_map))
	{
	    $new_items = $this->_get_new_items($from_ndx, &$plugin_cookies);
	    $this->_map = array_merge($this->_map, $new_items);
	}
	return $this->_map;
    }


#################################################

    private function _get_new_items($from_ndx, &$plugin_cookies)
    {
	hd_print("call $this->_reload_map_func(from_ndx=$from_ndx)");
	$items = call_user_func_array(array($this->_obj4func, $this->_reload_map_func), array($this, $from_ndx, $plugin_cookies));
	foreach($items as $c)
	    if($this->path === '')
		$c->path = (string)$c->id;
	    else
		$c->path = $this->path . '/' . $c->id;
	return $items;
    }

}

?>

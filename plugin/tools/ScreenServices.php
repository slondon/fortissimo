<?php

require_once 'lib/abstract_regular_screen.php';

require_once 'tools/exceptions.php';
require_once 'tools/service_cfg.php';
require_once 'tools/node_storage.php';


class ScreenServices extends AbstractRegularScreen implements UserInputHandler
{
    const ID = 'services';

    private $_node_storage_map;




    public function __construct(&$node_storage_map)
    {
	parent::__construct(self::ID, self::get_folder_views());
	UserInputHandlerRegistry::get_instance()->register_handler($this);
	$this->_node_storage_map = $node_storage_map;
    }

    public function get_handler_id()
    {
	return self::ID;
    }

    public function get_folder_range(MediaURL $media_url, $from_ndx, &$plugin_cookies)
    {
	$ns = $media_url->__get('ns');
	return $this->_node_storage_map[$ns]->get_folder_range($media_url, $from_ndx, $plugin_cookies);
    }


    static function get_folder_views()
    {
	$view_0 = array
	(
	    PluginRegularFolderView::view_params => array
	    (
		ViewParams::num_cols => 4,
		ViewParams::num_rows => 2,
		ViewParams::icon_selection_box_width => 220,
		ViewParams::icon_selection_box_height => 220,
		ViewParams::paint_details => true,
		ViewParams::item_detailed_info_auto_line_break => true,
		ViewParams::item_detailed_info_text_color => 23,
	    ),
	    PluginRegularFolderView::base_view_item_params => array
	    (
		ViewItemParams::icon_valign => VALIGN_CENTER,
		ViewItemParams::icon_width => 200,
		ViewItemParams::icon_height => 200,
		ViewItemParams::icon_dy => 15,
		ViewItemParams::icon_keep_aspect_ratio => true,
		ViewItemParams::item_caption_font_size => 1,
	    ),
	    PluginRegularFolderView::async_icon_loading => false,
	    PluginRegularFolderView::not_loaded_view_item_params => array(),
	);

        return array($view_0);
    }




    public function get_action_map(MediaURL $media_url, &$plugin_cookies)
    {
	$actions = array
	(
	    GUI_EVENT_KEY_ENTER => ActionFactory::open_folder(),
	    GUI_EVENT_KEY_C_YELLOW => Common::action_changelog($this),
	);

	$act_info_green = Common::action_info_green($this);
	if(!is_null($act_info_green))
	    $actions[GUI_EVENT_KEY_B_GREEN] = $act_info_green;
	return $actions;
    }

    public function handle_user_input(&$user_input, &$plugin_cookies)
    {
	if (isset($user_input->control_id))
	{
	    $control_id = $user_input->control_id;
	    switch ($control_id)
	    {
		case 'changelog':			return Common::get_changelog_dialog();
		case 'info_green':			return Common::get_info_green_dialog();
	    }
	}
	return array
	(
	    GuiAction::handler_string_id => PLUGIN_OPEN_FOLDER_ACTION_ID,
	    GuiAction::data => array
	    (
		'media_url' => $user_input->selected_media_url
	    ),
	);
    }
}

?>


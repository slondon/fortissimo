<?php


function get_filesize_str($size)
{
	return get_num_and_suffix($size) . "iB";
}

function get_traf_speed_str($size)
{
	return get_num_and_suffix($size) . "bps";
}


function get_num_and_suffix($size)
{
	if( $size < 1024 )
	{
	    $size_num = $size;
	    $size_suf = "B";
	}
	else if( $size < 1048576 ) // 1M
	{
	    $size_num = round($size / 1024, 2);
	    $size_suf = "K";
	}
	else if( $size < 1073741824 ) // 1G
	{
	    $size_num = round($size / 1048576, 2);
	    $size_suf = "M";
	}
	else if( $size < 1099511627776 ) // 1T
	{
	    $size_num = round($size / 1073741824, 2);
	    $size_suf = "G";
	}
	else
	{
	    $size_num = round($size / 1099511627776, 2);
	    $size_suf = "T";
	}
	return "$size_num $size_suf";
}




function seconds2hms($seconds)
{
    $s = intval($seconds);
    $fmt = 'H:i:s';
    if($s < 3600)
	$fmt = 'i:s';
    return gmdate($fmt, $s);
}


function create_dir($dir)
{
    $path_arr = explode('/', $dir);
    $curr_path = '';
    foreach($path_arr as $item)
    {
	if($item === '')
	    continue;
	$curr_path .= '/' . $item;
	if(!is_dir($curr_path))
	{
	    if(!mkdir($curr_path, 0755, true))
		return array
		(
		    'rc'	=> false,
		    'dir'	=> $curr_path,
		);
	    hd_print("mkdir: $curr_path");
        }
    }
    return array
    (
	'rc'	=> true,
	'dir'	=> '',
    );
}

?>

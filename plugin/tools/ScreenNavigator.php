<?php

require_once 'lib/abstract_regular_screen.php';

require_once 'tools/exceptions.php';
require_once 'tools/service_cfg.php';
require_once 'tools/node_storage.php';


class ScreenNavigator extends AbstractRegularScreen implements UserInputHandler
{
    const ID = 'navigator';

    private $_node_storage_map;
    private $_media_url_fav_list;



    public function __construct(&$node_storage_map)
    {
	parent::__construct(self::ID, self::get_folder_views());
	UserInputHandlerRegistry::get_instance()->register_handler($this);
	$this->_node_storage_map = $node_storage_map;

	$this->_media_url_fav_list = MediaURL::encode
	(
	    array
	    (
		'screen_id' => self::ID,
		'ns' => 'fav',
	    )
	);
	
    }

    public function get_handler_id()
    {
	return self::ID;
    }

    public function get_folder_range(MediaURL $media_url, $from_ndx, &$plugin_cookies)
    {
	$ns = $media_url->__get('ns');
	return $this->_node_storage_map[$ns]->get_folder_range($media_url, $from_ndx, $plugin_cookies);
    }


    static function get_folder_views()
    {
	$view_0 = array
	(
	    PluginRegularFolderView::view_params => array
	    (
	        ViewParams::num_cols => 1,
		ViewParams::num_rows => 10,
		ViewParams::paint_details => true,
		ViewParams::paint_item_info_in_details => true,
		ViewParams::zoom_detailed_icon => true,
		ViewParams::detailed_icon_scale_factor => 1,
		ViewParams::item_detailed_info_text_color => 23,
		ViewParams::item_detailed_info_auto_line_break => true
	    ),
	    PluginRegularFolderView::base_view_item_params => array
	    (
		ViewItemParams::item_paint_icon => true,
		ViewItemParams::icon_valign => VALIGN_TOP,
		ViewItemParams::item_layout => HALIGN_LEFT,
		ViewItemParams::icon_dx => 20,
		ViewItemParams::icon_sel_dx => 10,
		ViewItemParams::icon_keep_aspect_ratio => true,
		ViewItemParams::icon_scale_factor => 0.8,
		ViewItemParams::icon_sel_scale_factor => 1,
		ViewItemParams::item_caption_dx => 80,
	    ),
	    PluginRegularFolderView::not_loaded_view_item_params => array
	    (
		ViewItemParams::item_paint_icon => true,
		ViewItemParams::item_detailed_icon_path => 'missing://',
		ViewItemParams::icon_path => 'gui_skin://osd_icons/wait.aai',
	    ),
	    PluginRegularFolderView::async_icon_loading => true,
	    PluginRegularFolderView::initial_range => array(),
	);

        return array($view_0);
    }


    public function get_action_map(MediaURL $media_url, &$plugin_cookies)
    {
	$actions = array
	(
	    GUI_EVENT_KEY_ENTER => ActionFactory::open_folder(),
	    GUI_EVENT_KEY_C_YELLOW => Common::action_changelog($this),
	);

	$act_info_green = Common::action_info_green($this);
	if(!is_null($act_info_green))
	    $actions[GUI_EVENT_KEY_B_GREEN] = $act_info_green;


	if($media_url->__get('ns') === 'services')
	{
	    $fav = UserInputHandlerRegistry::create_action($this, "fav_add");
	    $fav['caption'] = "Favorite";
	    $actions[GUI_EVENT_KEY_D_BLUE] = $fav;
	}

	if($media_url->__get('ns') === 'fav' && is_null($media_url->__get('path')))
	{
	    $fav_delete_act = UserInputHandlerRegistry::create_action($this, 'fav_delete');
	    $fav_delete_act['caption'] = 'Delete';
	    $menu_items[] = array
	    (
		GuiMenuItemDef::caption	=> $fav_delete_act['caption'],
		GuiMenuItemDef::action	=> $fav_delete_act,
	    );
	    $actions[GUI_EVENT_KEY_POPUP_MENU] = ActionFactory::show_popup_menu($menu_items);
	}

	return $actions;
    }

    public function handle_user_input(&$user_input, &$plugin_cookies)
    {
	if (isset($user_input->control_id))
	{
	    $control_id = $user_input->control_id;
	    switch ($control_id)
	    {
		case 'changelog':		return Common::get_changelog_dialog();
		case 'info_green':		return Common::get_info_green_dialog();
		case 'fav_add':			return $this->_add_favorite($user_input, $plugin_cookies);
		case 'fav_delete':		return $this->_delete_favorite($user_input, $plugin_cookies);
	    }
	}
	return array
	(
	    GuiAction::handler_string_id => PLUGIN_OPEN_FOLDER_ACTION_ID,
	    GuiAction::data => array
	    (
		'media_url' => $user_input->selected_media_url
	    ),
	);
    }


#############################################################################

    private function _add_favorite($user_input, &$plugin_cookies)
    {
	$defs = array();
	$title = '';

	$text = 'Channel has been added to favorites:';
	$media_url = MediaUrl::decode($user_input->selected_media_url);
	if($media_url->__get('screen_id') === 'rss' && $media_url->__get('ns') === 'services')
	{
	    $path = $media_url->__get('path');
	    $node = $this->_node_storage_map['services']->get_node($path, $plugin_cookies);
	    $rc = FavoritesCtl::add_item($node, $title);
	    if(! $rc)
		$text = 'ERROR: can\'t add to favorites';
	}
	else
	    $text = 'ERROR: restricted operation for this type of items';

	ControlFactory::add_label($defs, "", '');
	ControlFactory::add_label($defs, "", $text);
	if($title !== '')
	    ControlFactory::add_label($defs, "", "'$title'");
	ControlFactory::add_label($defs, "", '');

	ControlFactory::add_custom_close_dialog_and_apply_button($defs, 'fav_add', 'Ok', 150,  null);
	$act_show_dialog = ActionFactory::show_dialog("Adding to favorites", $defs);
	return ActionFactory::invalidate_folders(array($this->_media_url_fav_list), $act_show_dialog);
    }

    private function _delete_favorite($user_input, &$plugin_cookies)
    {
	$defs = array();
	$title = '';

	$text = 'Channel has been deleted from favorites:';
	$media_url = MediaUrl::decode($user_input->selected_media_url);
	$path = $media_url->__get('path');
	$idx = (int)$path;
	$rc = FavoritesCtl::delete_item($idx);
	if(! $rc)
	    $text = 'ERROR: can\'t delete from favorites';

	$node = $this->_node_storage_map['fav']->get_node($path, $plugin_cookies);
	$title = $node->name;
	ControlFactory::add_label($defs, "", '');
	ControlFactory::add_label($defs, "", $text);
	if($title !== '')
	    ControlFactory::add_label($defs, "", "'$title'");
	ControlFactory::add_label($defs, "", '');

	$post_act = ActionFactory::invalidate_folders(array($user_input->parent_media_url));
	ControlFactory::add_custom_close_dialog_and_apply_button($defs, 'fav_delete', 'Ok', 150,  $post_act);
	return ActionFactory::show_dialog("Deleting from favorites", $defs);
    }

}

?>


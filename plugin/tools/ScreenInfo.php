<?php





class ScreenInfo implements Screen
{
    const ID = 'info';

    private $_screen_rss;
    private $_node_storage_map;


    public function __construct($screen_rss, &$node_storage_map)
    {
	$this->_screen_rss = $screen_rss;
	$this->_node_storage_map = $node_storage_map;
    }

    public function get_id()		{ return self::ID; }

    public function get_folder_view(MediaURL $media_url, &$plugin_cookies)
    {
	$ns = $media_url->__get('ns');
	$path = $media_url->__get('path');
	$node = $this->_node_storage_map[$ns]->get_node($path, $plugin_cookies);


	$rate_details = array();

	$descr = $node->info['description'];

	$duration = '';
	if(isset($node->info['duration']))
	{
	    $duration = $node->info['duration'];
	    if(strlen($duration) > 5)
	    {
		$rate_details['duration'] = '';
		$rate_details[''] = $duration;
	    }
	    else
		$rate_details['duration'] = $duration;
	}

	$minutes = 0;
	$dur_arr = explode(':', $duration);
	if(count($dur_arr) === 1)
	    $minutes = $dur_arr[0] / 60;
	else if(count($dur_arr) === 2)
	    $minutes = $dur_arr[0];
	else if(count($dur_arr) === 3)
	    $minutes = $dur_arr[0] * 60 + $dur_arr[1];



	$size_str = '';
	if(isset($node->info['file_size']))
	    $rate_details['size'] = $node->info['file_size'];


#	$media_url->__set('screen_id', 'rss');
		    $info = array
		    (
			PluginMovie::name		=> $node->name,
#			PluginMovie::name_original	=> '',
			PluginMovie::description	=> $descr,
			PluginMovie::poster_url		=> $node->icon,
			PluginMovie::length_min		=> $minutes,
			PluginMovie::year		=> '',
			PluginMovie::directors_str	=> '',
			PluginMovie::scenarios_str	=> '',
			PluginMovie::actors_str		=> '',
			PluginMovie::genres_str		=> $node->pub_date,
			PluginMovie::rate_imdb		=> '',
			PluginMovie::rate_kinopoisk	=> '',
			PluginMovie::rate_mpaa		=> '',
			PluginMovie::country		=> '',
			PluginMovie::budget		=> '',
			PluginMovie::details		=> array
			    (
			    ),
			PluginMovie::rate_details	=> $rate_details,
		    );
		
		    $info_view = array
		    (
			PluginMovieFolderView::movie			=> $info,
			PluginMovieFolderView::left_button_caption	=> 'Play',
			PluginMovieFolderView::left_button_action	=> ActionFactory::vod_play(),
			PluginMovieFolderView::has_right_button		=> true,
			PluginMovieFolderView::right_button_caption	=> 'Download',
			PluginMovieFolderView::right_button_action	=> UserInputHandlerRegistry::create_action($this->_screen_rss, 'req_download'),
			PluginMovieFolderView::has_multiple_series	=> false,
#			PluginMovieFolderView::series_media_url		=> $media_url,
			PluginMovieFolderView::series_media_url		=> NULL,
			PluginMovieFolderView::poster_valign		=> VALIGN_CENTER,
		    );
		
		    return array
		    (
			PluginFolderView::multiple_views_supported	=> false,
			PluginFolderView::archive			=> NULL,
			PluginFolderView::view_kind			=> PLUGIN_FOLDER_VIEW_MOVIE,
			PluginFolderView::data				=> $info_view,
		    );
    }

}


?>

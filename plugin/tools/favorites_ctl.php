<?php


require_once 'tools/node.php';


class FavoriteItem
{
    public $type;
    public $service;
    public $title;
    public $url;
    public $icon_path;
    public $description;

    public $id;
}


class FavoritesCtl
{
    const FILENAME = 'favorites.xml';
    const XML_BASE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><favorites></favorites>";



    static public function get_filename()
    {
	return DuneSystem::$properties['data_dir_path'] . '/' . self::FILENAME;
    }

    static public function load_file()
    {
	$filename = self::get_filename();
	if(file_exists($filename))
	{
	    $xml = simplexml_load_file($filename);
	    if(!$xml)
		$xml = new SimpleXMLElement(self::XML_BASE);
	}
	else
	    $xml = new SimpleXMLElement(self::XML_BASE);
	return $xml;
    }

    static public function add_item($node, &$title)
    {
	$xml = self::load_file();
	$item = $xml->addChild('item');
	$item->addChild('type', $node->type);
	$item->addChild('service', $node->get_obj_name());
	$title = '[' . htmlspecialchars($node->get_obj_title() . '] ' . $node->name);
	$item->addChild('title', htmlspecialchars($title));
	$item->addChild('url', htmlspecialchars($node->rss_url));
	$item->addChild('icon_path', htmlspecialchars($node->icon));
	$item->addChild('description', htmlspecialchars(Common::get_descr_str($node->descr)));

	$doc = new DOMDocument('1.0');
	$doc->formatOutput = true;
	$doc->preserveWhiteSpace = false;
	$doc->loadXML($xml->asXML());
	$doc->save(self::get_filename());

	return true;
    }

    static public function delete_item($idx)
    {
	$xml = self::load_file();


	$doc = new DOMDocument('1.0');
	$doc->formatOutput = true;
	$doc->preserveWhiteSpace = false;
	$doc->loadXML($xml->asXML());

	$item2del = $doc->documentElement->getElementsByTagName('item')->item($idx);
	$doc->documentElement->removeChild($item2del);

	$doc->save(self::get_filename());

	return true;
    }


    static public function load_fav_list($root_node, $plugin_cookies)
    {
	$fav_nodes = array();
	$fav_items = self::_get_items();
	foreach($fav_items as $f)
	{
	    $node_info = new NodeInfo();
	    $node_info->id = $f->id;
	    $node_info->path = '';
	    $node_info->type = $f->type;
	    $node_info->name = $f->title;
	    $node_info->rss_url = $f->url;
	    $node_info->icon = $f->icon_path;
	    $node_info->descr = $f->description;

	    $service = $f->service;
	    $include = implode("/", array("services", $service, $service . ".php"));

	    $file = DuneSystem::$properties['install_dir_path'] . "/" . $include;
	    clearstatcache(true, $file);
	    if( !file_exists($file) )
		continue;

	    require_once $include;
	    $class = $service . "Service";
	    $srv = new $class;

	    $func = Common::get_func_name4fav_by_type($f->type);
	    $node = new Node($node_info, $srv, $func);
	    $fav_nodes[$f->id] = $node;
	}
	return $fav_nodes;
    }


########################################################################

    static private function _get_items()
    {
	$items = array();
	$id = 0;
	$xml = self::load_file();
	foreach($xml as $i)
	{
	    $item = new FavoriteItem();
	    $item->type = $i->type->__toString();
	    $item->service = $i->service->__toString();
	    $item->title = htmlspecialchars_decode($i->title->__toString());
	    $item->url = htmlspecialchars_decode($i->url->__toString());
#	    $item->icon_path = htmlspecialchars_decode(Common::get_icon_path($i->icon_path->__toString()));
	    $item->icon_path = Common::get_icon_path($i->icon_path->__toString());
	    $item->description = array(htmlspecialchars_decode($i->description->__toString()));
	    $item->id = (string)$id;

	    $items[] = $item;
	    $id++;
	}
	return $items;
    }


}

?>

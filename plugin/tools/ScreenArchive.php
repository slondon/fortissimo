<?php

require_once 'lib/abstract_regular_screen.php';

require_once 'tools/Common.php';
require_once 'tools/ScreenNavigator.php';


class NodeAr
{
    public $id;
    public $path;
    public $type;
    public $name;
    public $children;

    public function __construct($id, $path_parent, $type, $name)
    {
	$this->id = $id;
	$this->path = '';
	if(strlen($id))
	{
	    if(strlen($path_parent))
		$this->path = $path_parent . '/' . $id;
	    else
		$this->path = $id;
	}
	$this->type = $type;
	$this->name = $name;
	$this->children = NULL;
    }
}


class ScreenArchive extends AbstractRegularScreen implements UserInputHandler
{
    const ID = 'archive';

    private $_node_storage;

    public function __construct()
    {
	parent::__construct(self::ID, self::get_folder_views());
	UserInputHandlerRegistry::get_instance()->register_handler($this);
	$this->_node_storage = new NodeAr('', '', 'dir', '');
    }

    public function get_handler_id()
    {
	return self::ID;
    }

    public function get_folder_range(MediaURL $media_url, $from_ndx, &$plugin_cookies)
    {
	$items = $this->_get_items_list($media_url, $from_ndx, $plugin_cookies);

	$count = count($items);
	return array
	(
	    PluginRegularFolderRange::total => $count,
	    PluginRegularFolderRange::more_items_available => false,
	    PluginRegularFolderRange::from_ndx => $from_ndx,
	    PluginRegularFolderRange::count => $count,
	    PluginRegularFolderRange::items => $items
	);
    }




    public function get_action_map(MediaURL $media_url, &$plugin_cookies)
    {
	$act_item = UserInputHandlerRegistry::create_action($this, 'act_item');
	$act_item['caption'] = 'act_item';

	$actions = array
	(
	    GUI_EVENT_KEY_ENTER => $act_item,
	    GUI_EVENT_KEY_C_YELLOW => Common::action_changelog($this),
	);

	$act_info_green = Common::action_info_green($this);
	if(!is_null($act_info_green))
	    $actions[GUI_EVENT_KEY_B_GREEN] = $act_info_green;

	return $actions;
    }

    public function handle_user_input(&$user_input, &$plugin_cookies)
    {
	if (isset($user_input->control_id))
	{
	    $control_id = $user_input->control_id;
	    switch ($control_id)
	    {
		case 'changelog':			return Common::get_changelog_dialog();
		case 'info_green':			return Common::get_info_green_dialog();
		case 'act_item':
		{
		    $media_url = MediaUrl::decode($user_input->selected_media_url);
		    $item_type = $media_url->__get('type');
		
		    $act_open_folder = array
		    (
			GuiAction::handler_string_id => PLUGIN_OPEN_FOLDER_ACTION_ID,
			GuiAction::data => array
			(
			    'media_url' => $user_input->selected_media_url
			),
		    );

		    $action4item = $act_open_folder;

		    if($item_type === 'audio')
		    {
			$act_play_item = ActionFactory::vod_play();
			$action4item = $act_play_item;
		    }

		    return ActionFactory::invalidate_folders(
			array
			(
			    $user_input->selected_media_url,
#			    $user_input->parent_media_url
			),
			$action4item);
		}
	    }
	}

	return ActionFactory::invalidate_folders(array());
    }


    static function get_folder_views()
    {
	$view_0 = array
	(
	    PluginRegularFolderView::view_params => array
	    (
	        ViewParams::num_cols => 1,
		ViewParams::num_rows => 10,
#		ViewParams::paint_details => true,
#		ViewParams::paint_item_info_in_details => true,
#		ViewParams::zoom_detailed_icon => true,
#		ViewParams::detailed_icon_scale_factor => 1,
#		ViewParams::item_detailed_info_text_color => 23,
#		ViewParams::item_detailed_info_auto_line_break => true
	    ),
	    PluginRegularFolderView::base_view_item_params => array
	    (
		ViewItemParams::item_paint_icon => true,
		ViewItemParams::icon_valign => VALIGN_TOP,
		ViewItemParams::item_layout => HALIGN_LEFT,
		ViewItemParams::icon_dx => 20,
		ViewItemParams::icon_sel_dx => 10,
		ViewItemParams::icon_keep_aspect_ratio => true,
		ViewItemParams::icon_scale_factor => 0.8,
		ViewItemParams::icon_sel_scale_factor => 1,
		ViewItemParams::item_caption_dx => 80,
	    ),
	    PluginRegularFolderView::not_loaded_view_item_params => array
	    (
		ViewItemParams::item_paint_icon => true,
		ViewItemParams::item_detailed_icon_path => 'missing://',
		ViewItemParams::icon_path => 'gui_skin://osd_icons/wait.aai',
	    ),
#	    PluginRegularFolderView::async_icon_loading => true,
	    PluginRegularFolderView::initial_range => array(),
	);

        return array($view_0);
    }


    public function get_vod_info(MediaURL $media_url, &$plugin_cookies)
    {
	$path = $media_url->__get('path');

	$curr_node = $this->_node_storage;
	$curr_file = $curr_node->name;

	$path_arr = explode('/', $path);
	foreach($path_arr as $p)
	{
	    $child = $curr_node->children[$p];
	    $curr_node = $child;
	    $curr_file .= '/' . $curr_node->name;
	}

	$full_path = ScreenSettings::get_store_directory($plugin_cookies);
	if(!$full_path)
	    return array();
	$full_path .= '/' . $curr_file;

	$file = basename($full_path);

	return array
	(
	    PluginVodInfo::name		=> $file,
	    PluginVodInfo::description	=> $file,
	    PluginVodInfo::poster_url	=> 'missing://',
	    PluginVodInfo::series	=> array
	    (
		array
		(
		    PluginVodSeriesInfo::name		=> '',
		    PluginVodSeriesInfo::playback_url	=> $full_path,
		    PluginVodSeriesInfo::playback_url_is_stream_url => false
		)
	    )
	);
    }




###################################################

    private function _get_items_list(MediaURL $media_url, $from_ndx, &$plugin_cookies)
    {
	$items_dir = array();
	$items_file = array();

	$path = $media_url->__get('path');

	$curr_node = $this->_node_storage;
	$curr_file = $curr_node->name;

	if(strlen($path))
	{
	    $path_arr = explode('/', $path);
	    foreach($path_arr as $p)
	    {
		$child = $curr_node->children[$p];
		$curr_node = $child;
		$curr_file .= '/' . $curr_node->name;
	    }
	}

	$full_path = ScreenSettings::get_store_directory($plugin_cookies);
	if(!$full_path)
	    return array();
	$full_path .= '/' . $curr_file;

	$fd = dir($full_path);
	if($fd)
	{
	    while(false !== ($name = $fd->read()))
	    {
		if( $name === "." || $name === ".." )
		    continue;

		$file_path = implode('/', array($full_path, $name));

		$type = is_dir($file_path) ? 'dir' : 'audio';

		$item = array
		(
		    'name'	=> $name,
		    'type'	=> $type,
		    'file_path'	=> $file_path,
		);
		if($type === 'dir')
		    $items_dir[$name] = $item;
		else
		    $items_file[$name] = $item;
	    }
	}

	ksort($items_dir);
	krsort($items_file);

	$items = array();
	$idx = 0;
	$curr_node->children = array();
	foreach(array_merge($items_dir, $items_file) as $item)
	{
	    $new_node = new NodeAr($idx, $curr_node->path, $item['type'], $item['name']);

	    $item_media_url = MediaURL::encode(array('screen_id' => self::ID, 'type' => $type, 'path' => $new_node->{'path'}));
	    $item = array
	    (
		PluginRegularFolderItem::caption		=> $item['name'],
		PluginRegularFolderItem::media_url		=> $item_media_url,
		PluginRegularFolderItem::view_item_params	=> array
		(
		    ViewItemParams::icon_path		=> Common::get_icon4type($type),
		),
	    );
	    $curr_node->children[$idx] = $new_node;
	    $items[] = $item;
	    $idx++;
	}

	return $items;
    }

}

?>


<?php


require_once 'exceptions.php';




class RssParser
{

    static public function load_rss($url, $params)
    {
	hd_print("parse rss_url=$url");
	$rss = array();
#	$raw_data = file_get_contents( $url );
	hd_print("doc has been downloaded");
	$node = NULL;
	try
	{
	    $node = new SimpleXMLElement($url, LIBXML_COMPACT, true);
#	    $node = simplexml_load_string($raw_data);
	}
	catch (Exception $e)
	{
	    hd_print("xml exception");
	}
	if(!$node)
	{
	    hd_print("xml error:");
	    return array();
	}



	$xml_item_path = explode('/', $params['schema']['xml_item_path']);
	$first_item = array_shift($xml_item_path);
	if($first_item !== $node->getName())
	{
	    hd_print("parse xml error: first element is '" . $node->getName(). "' while expected '" . $first_item . "'");
	    return array();
	}


	$count = count($xml_item_path);
	$i = 0;
	foreach($xml_item_path as $path)
	{
	    $i++;
	    foreach($node->children() as $child)
	    {
		if($child->getName() === $path)
		{
		    if($i !== $count)
		    {
			$node = $child;
			break;
		    }
		    else
		    {
			$out = array();
			foreach($params['schema'] as $k => $v)
			{
			    if(strncmp($k, 'item_', 5) === 0)
			    {
				$arr = explode('|', $v);
				$element = $arr[0];
				$attribute = count($arr) > 1 ? $attribute = $arr[1] : '';
			    
				$tmp = explode(':', $element);
				$ns = $tmp[0];
				$ns_item = isset($tmp[1]) ? $tmp[1] : '';
				if($ns_item !== '')
				{
				    foreach($child->children($ns, true) as $c)
				    {
					if($c->getName() === $ns_item)
					{
					    if(strlen($attribute) > 0)
					    {
						$attrs = $c->attributes();
						foreach($attrs as $attr_k => $attr_v)
						{
						    if($attr_k === $attribute)
						    {
							$out[$k] = (string) $attr_v;
							break;
						    }
						}
					    }
					    else
					    {
						$out[$k] = (string) $c;
					    }
					}
				    }
				    continue;
				}
			    
			    
				$tmp = explode('|', $v);
				$element = $tmp[0];
				$attribute = isset($tmp[1]) ? $tmp[1] : '';
				$item = $child->$element;
				$value_str = (string) $item;
				if($attribute !== '')
				    $value_str = (string) $item[$attribute];
				$out[$k] = $value_str;
			    }
			}
			$rss[] = $out;
		    }
		}
	    }
	}


	hd_print("parse end");
	return $rss;
    }

}

?>


<?php



class Downloader
{
    private $_node_storage_map;


    private $_file_part_full;
    private $_file_part;
    private $_file_full;
    private $_file_name;
    private $_start_time;
    private $_stop_time;
    private $_proc_handle;
    private $_proc_pipes;


    public function __construct(&$node_storage_map)
    {
	$this->_node_storage_map = $node_storage_map;
	$this->_proc_handle = NULL;
    }

    public function start_download_file($file_url, $path, $name, $store_dir)
    {
	hd_print("download: url=$file_url, path=$path, name=$name, store_dir=$store_dir");

	if(!is_dir($store_dir))
	    return array
	    (
		'completed'	=> true,
		'file'		=> '',
		'size'		=> 0,
		'error'		=> "place doesn't exists",
	    );

	if(!self::check_mounted($store_dir))
	    return array
	    (
		'completed'	=> true,
		'file'		=> '',
		'size'		=> 0,
		'error'		=> 'destination is not a disk',
	    );

	$res = create_dir($path);
	if(!$res['rc'])
	    return array
	    (
		'completed'	=> true,
		'file'		=> '',
		'size'		=> 0,
		'error'		=> "can't create dir: " . $res['dir'],
	    );

	$path_arr = explode('/', $path);
	$curr_path = '/';
	foreach($path_arr as $item)
	{
	    $curr_path .= '/' . $item;
	    if(!is_dir($curr_path))
	    {
		if(!mkdir($curr_path, 0755, true))
		    return array
		    (
			'completed'	=> true,
			'file'		=> '',
			'size'		=> 0,
			'error'		=> "can't create dir: $curr_dir",
		    );
		hd_print("mkdir: $curr_path");
	    }
	}

	$file = substr($name, 0, 350 - strlen("$path/.part"));
	$file_part = "$file.part";
	$file_on_disk = "$path/$file";
	$file_on_disk_part = "$path/$file.part";

	hd_print("download: file_on_disk_part=$file_on_disk_part");



	$this->_file_part_full = $file_on_disk_part;
	$this->_file_part = $file_part;
	$this->_file_full = $file_on_disk;
	$this->_file_name = $file;

	$wget_out_file = addslashes($this->_file_part_full);
#	$cmd = "wget -O \"$wget_out_file\" \"$file_url\"";
#	$cmd = "/codecpack/bin/wget --no-check-certificate -O \"$wget_out_file\" \"$file_url\"";
	$cmd = DuneSystem::$properties['install_dir_path'] . "/bin/wget --no-check-certificate -O \"$wget_out_file\" \"$file_url\"";
	$this->_start_time = microtime(true);
	$this->_stop_time = NULL;
	$this->_percent = 0;


	$descs = array
	(
	    0 => array ('pipe', 'r'),
	    1 => array ('pipe', 'w'),
	    2 => array ('pipe', 'w')
	);
	$this->_proc_pipes = array();
        $this->_proc_handle = proc_open($cmd, $descs, $this->_proc_pipes);
	if(!$this->_proc_handle)
	    return array
	    (
		'completed'	=> true,
		'file'		=> $file,
		'size'		=> 0,
		'error'		=> "can't execute 'wget' utility",
	    );
	stream_set_blocking($this->_proc_pipes[2], 0);

	hd_print("downloading: $cmd");
	return array
	(
	    'completed'	=> false,
	    'file'	=> $file_part,
	    'size'	=> 0,
	);
    }


    public function get_state_download_file()
    {
	if(!$this->_proc_handle)
	    return array
	    (
		'completed'	=> true,
		'file'		=> $this->_file_name,
		'size'		=> 0,
		'error'		=> 'internal error: _proc_handle == NULL',
	    );

	do
	{
	    if(!feof($this->_proc_pipes[2]))
	    {
		$tmp = '';
		do
		{
		    $prev = $tmp;
#		    $tmp = stream_get_line($this->_proc_pipes[2], 1024, "\r");
		    $tmp = stream_get_line($this->_proc_pipes[2], 1024, "\n");
		}
		while(strlen($tmp));
		$str = $prev;
#		hd_print("wget output: $str");

#		$pattern = "/\s+(\d+)%\s+\|[ \*]+\|\s+\w+\s+([\:\d\-]+)\s+ETA/";
#		$pattern = "/\s*(\d+)%\s*\[.+\].+eta (\d+)s/";
		$pattern = "/^ +\d+[KMG] .+(\d+)%.+ (\d+)s/";

		$pattern1 = "/^ +\d+[KMG] .+/";
		if(preg_match($pattern1, $str, $out) === 1)
		{
			$pattern2 = "/[ .](\d+)%.+ (\d+)s/";
			$pattern3 = "/(100)% .+=(\d+)s/";
			if(preg_match($pattern2, $str, $out) === 1 || preg_match($pattern3, $str, $out) === 1)
			{
				$this->_stop_time = microtime(true);
				$elapsed = (int) ($this->_stop_time - $this->_start_time);
				$remain = '00:00:00';
				if($out[2] !== '--:--:--')
				{
					$sec = $out[2];
					$remain = strftime("%T", $sec);
					$h = floor($sec / 3600);
					$m = floor(($sec - ($h * 3600)) / 60);
					$s = $sec - $h * 3600 - $m * 60;
#					$remain = "$h:$m:$s";
#					$remain = $out[2];
				}

				$size = $this->_get_file_size($this->_file_part_full);
				$traf_speed = $this->_get_traf_speed($size);

				$this->_percent = $out[1];
#				if($this->_percent === '100' && feof($this->_proc_pipes[2]))
				if($this->_percent === '100')
					break;
				return array
				(
					'completed'		=> false,
					'file'			=> $this->_file_part,
					'size'			=> $size,
					'percent'		=> $this->_percent,
					'remain'		=> $remain,
					'elapsed'		=> $elapsed,
					'traf_speed'	=> $traf_speed,
				);
			}
		}
		if(feof($this->_proc_pipes[2]))
		    break;
		return array
		(
		    'completed'	=> false,
		    'file'	=> $this->_file_part,
		    'size'	=> 0,
		);
	    }
	}
	while(false);

	$this->_stop_time = microtime(true);
	$elapsed = (int) ($this->_stop_time - $this->_start_time);
	$rc = proc_close($this->_proc_handle);
	$this->_proc_handle = NULL;

	if($rc)
	{
	    return array
	    (
		'completed'	=> true,
		'file'		=> $this->_file_part,
		'size'		=> 0,
		'error'		=> "'wget' operation failed",
	    );
	}

	clearstatcache(false, $this->_file_part_full);
	if(!is_file($this->_file_part_full))
	{
	    return array
	    (
		'completed'	=> true,
		'file'		=> $this->_file_part,
		'size'		=> 0,
		'error'		=> "downloaded file doesn't exists after 'wget' completed",
	    );
	}


	$size = $this->_get_file_size($this->_file_part_full);

	if(!rename($this->_file_part_full, $this->_file_full))
	    return array
	    (
		'completed'	=> true,
		'file'		=> $this->_file_part,
		'size'		=> $size,
		'error'		=> 'can\'t rename to file ' . $this->_file_name,
	    );
	hd_print("rename: '$this->_file_part_full' to '$this->_file_full'");


	$size = $this->_get_file_size($this->_file_full);
	$traf_speed = $this->_get_traf_speed($size);

	hd_print("downloaded: file=$this->_file_full, size=$size");
	return array
	(
	    'completed'	=> true,
	    'file'	=> $this->_file_name,
	    'size'	=> $size,
	    'percent'	=> 100,
	    'elapsed'	=> $elapsed,
	    'traf_speed'=> $traf_speed,
	);
    }


    public function cancel_downloading()
    {
	hd_print("cancel downloading file: " . $this->_file_full);
	if($this->_proc_handle)
	{
	    proc_terminate($this->_proc_handle);

	    $waiting_start_time = microtime(true);
	    do
	    {
		$res = proc_get_status($this->_proc_handle);
		if(!$res || !$res['running'])
		    break;
		$curr_time = microtime(true);
	    }
	    while($curr_time - $waiting_start_time < 1.0);

	    $this->_proc_handle = NULL;
	    if(is_file($this->_file_part_full))
		unlink($this->_file_part_full);
	    if(is_file($this->_file_full))
		unlink($this->_file_full);
	}
    }


    public function get_store_path($ns, $item_path, &$plugin_cookies)
    {
	$node_storage = $this->_node_storage_map[$ns];
	$node = $node_storage->get_node($item_path, $plugin_cookies);

	$item_path_arr = explode('/', $item_path);
	array_pop($item_path_arr);
	$rss_path = implode('/', $item_path_arr);
	$rss_node = $node_storage->get_node($rss_path, $plugin_cookies);


	if($ns === 'fav')
	{
	    preg_match('|\[(.+)\] (.+)|', $rss_node->name, $out);
	    $service = $out[1];
	    $rss = $out[2];
	}
	else if($ns === 'services')
	{
	    $service = $node->get_obj_title();

	    $rss = $rss_node->name;
	}
	hd_print("service=$service, rss=$rss");



	$dir = ScreenSettings::get_store_directory($plugin_cookies);
	$subdir_tmpl = ScreenSettings::get_store_subdir($plugin_cookies);

	$patterns = array();
	$patterns[0] = '|service|';
	$patterns[1] = '|rss|';
	$patterns[2] = '|file|';
	$replacements = array();
	$replacements[0] = $service;
	$replacements[1] = $rss;
	$replacements[2] = '';
	$subdir = preg_replace($patterns, $replacements, "$subdir_tmpl");

	$path = "$dir/$subdir";

	hd_print("store path: " . $path);
	return array
	(
	    'path'	=> $path,
	    'dir'	=> $dir,
	    'subdir'	=> $subdir,
	    'service'	=> $service,
	    'rss'	=> $rss,
	);
    }


    static public function check_mounted($path)
    {
	$last_line = system("mountpoint -d \"$path\"", $rc);
	if(strpos($last_line, '8:') === 0)
	    return true;
	if(strpos($last_line, '179:') === 0)
	    return true;
	return false;
    }


    public function _get_file_size($file)
    {
	clearstatcache(false, $file);
	if(!is_file($file))
	    return 0;
	return filesize($file);
    }

    public function _get_traf_speed($size)
    {
	if(!$this->_stop_time)
	    return '';
	$time = $this->_stop_time - $this->_start_time;
	if(!$time)
	    return '';
	return get_traf_speed_str($size * 8 / $time);
    }

}


?>

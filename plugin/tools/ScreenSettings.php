<?php
///////////////////////////////////////////////////////////////////////////

require_once 'lib/abstract_controls_screen.php';
require_once 'lib/text_utils.php';

///////////////////////////////////////////////////////////////////////////

class ScreenSettings extends AbstractControlsScreen
{
    const ID = 'settings';

    const STORAGE_BASE_DIR = '/tmp/mnt/storage';

    // some defaults
    const ITEM_VIEW_CAPTION_DATE_FORMAT = 'd M H:i';

    const ITEM_VIEW_DETAIL_POSTER = 'yes';
    const ITEM_VIEW_DETAIL_TITLE = 'yes';
    const ITEM_VIEW_DETAIL_START_TIME = 'yes';
    const ITEM_VIEW_DETAIL_DURATION = 'yes';
    const ITEM_VIEW_DETAIL_FILE_SIZE = 'no';
    const ITEM_VIEW_DETAIL_DESCRIPTION = 'yes';

    const ACTION_FOR_ENTER = 'play';
    const ACTION_FOR_PLAY = 'play';

    const STORE_DISK = 'none';
    const STORE_DIR = 'none';
    const STORE_SUBDIR = 'file';
    const SEARCH_STORE_DIR_DEEP = 2;
    const STORE_DATE_FORMAT = 'ymd_Hi';
    const STORE_TIMEOUT_PLAY = 2;

    const CHECK_REAL_URL = 'no';


    private $_opts_yes_no;
    private $_opts_action;
    private $_opts_store_subdir;
    private $_opts_store_date_fmt;
    private $_opts_iv_caption_date_fmt;


    public function __construct()
    {
        parent::__construct(self::ID);
        $this->_set_combobox_options();
    }


###################################

    static public function get_item_view_caption_date_format(&$plugin_cookies)
    {
	self::set_default_values($plugin_cookies);
	return $plugin_cookies->setup_iv_caption_date_fmt;
    }

    static public function need_item_view_detail_poster(&$plugin_cookies)
    {
	self::set_default_values($plugin_cookies);
	return $plugin_cookies->setup_iv_detail_poster === 'yes';
    }

    static public function need_item_view_detail_title(&$plugin_cookies)
    {
	self::set_default_values($plugin_cookies);
	return $plugin_cookies->setup_iv_detail_title === 'yes';
    }

    static public function need_item_view_detail_start_time(&$plugin_cookies)
    {
	self::set_default_values($plugin_cookies);
	return $plugin_cookies->setup_iv_detail_start_time === 'yes';
    }

    static public function need_item_view_detail_duration(&$plugin_cookies)
    {
	self::set_default_values($plugin_cookies);
	return $plugin_cookies->setup_iv_detail_duration === 'yes';
    }

    static public function need_item_view_detail_file_size(&$plugin_cookies)
    {
	self::set_default_values($plugin_cookies);
	return $plugin_cookies->setup_iv_detail_file_size === 'yes';
    }

    static public function need_item_view_detail_descr(&$plugin_cookies)
    {
	self::set_default_values($plugin_cookies);
	return $plugin_cookies->setup_iv_detail_descr === 'yes';
    }




    static public function get_act_enter(&$plugin_cookies)
    {
	self::set_default_values($plugin_cookies);
	return $plugin_cookies->setup_act_enter;
    }

    static public function get_act_play(&$plugin_cookies)
    {
	self::set_default_values($plugin_cookies);
	return $plugin_cookies->setup_act_play;
    }

    static public function get_store_place(&$plugin_cookies)
    {
	self::set_default_values($plugin_cookies);
	return "$plugin_cookies->setup_store_disk://$plugin_cookies->setup_store_dir";
    }

    static public function get_store_subdir(&$plugin_cookies)
    {
	self::set_default_values($plugin_cookies);
	return $plugin_cookies->setup_store_subdir;
    }

    static public function get_store_directory(&$plugin_cookies)
    {
	self::set_default_values($plugin_cookies);
	if(	$plugin_cookies->setup_store_disk !== 'none'
	    &&	$plugin_cookies->setup_store_dir !== 'none' )
	{
	    $dir = implode("/", array(self::STORAGE_BASE_DIR, $plugin_cookies->setup_store_disk, $plugin_cookies->setup_store_dir));
	    return $dir;
	}
	return null;
    }

    static public function get_store_date_format(&$plugin_cookies)
    {
	self::set_default_values($plugin_cookies);
	return $plugin_cookies->setup_store_date_fmt;
    }


    static public function get_store_timeout_play(&$plugin_cookies)
    {
	self::set_default_values($plugin_cookies);
	return $plugin_cookies->setup_store_timeout_play;
    }

    static public function need_check_real_url(&$plugin_cookies)
    {
	self::set_default_values($plugin_cookies);
	return $plugin_cookies->setup_check_real_url === 'yes';
    }

###################################



    public function do_get_control_defs(&$plugin_cookies)
    {
        $defs = array();

	$version = "unknown";
	$ver_file = DuneSystem::$properties['install_dir_path'] . "/version";
	if( file_exists($ver_file) )
	{
	    $str = file_get_contents( $ver_file );
	    if( $str !== null )
		$version = trim($str);
	}
	$this->add_label($defs, "plugin:", "foRtiSSimo");
	$this->add_label($defs, "description:", "listen RSS audio podcasts");
	$this->add_label($defs, "version:", $version);

	$this->set_default_values($plugin_cookies);

	$this->add_button($defs, 'setup_item_view_format', "Item view format:", "configure...", -1);
	$this->add_combobox($defs, 'setup_act_enter', "Action for 'ENTER':", $plugin_cookies->setup_act_enter, $this->_opts_action, 0, true);
	$this->add_combobox($defs, 'setup_act_play', "Action for 'PLAY':", $plugin_cookies->setup_act_play, $this->_opts_action, 0, true);
	$this->add_button($defs, 'setup_store_options', "Store files options:", "view and change...", -1);
	$this->add_combobox($defs, 'setup_check_real_url', "Check real URL:", $plugin_cookies->setup_check_real_url, $this->_opts_yes_no, 0, true);

	return $defs;
    }

    public function get_control_defs(MediaURL $media_url, &$plugin_cookies)
    {
	return $this->do_get_control_defs($plugin_cookies);
    }

    public function handle_user_input(&$user_input, &$plugin_cookies)
    {
	hd_print('Setup: handle_user_input:');
	foreach ($user_input as $key => $value)
	hd_print("  $key => $value");

	if( $user_input->action_type === 'confirm' )
	{
	    $reset_controls = false;
	    $control_id = $user_input->control_id;
	    $new_value = $user_input->{$control_id};
	    hd_print("Setup: changing $control_id value to $new_value");


	    if( $control_id === 'setup_store_disk' )
	    {
		if( $plugin_cookies->setup_store_disk !== $new_value )
		    $plugin_cookies->setup_store_dir = 'none';
		$plugin_cookies->setup_store_disk = $new_value;
	    }
	    else if($control_id === 'setup_iv_detail_poster')
		$plugin_cookies->need_update_screen = true;

	    $plugin_cookies->{$control_id} = $new_value;


	    if($user_input->selected_control_id === 'setup_store_options')
		return ActionFactory::reset_controls( $this->do_get_dialog_store_options_defs($plugin_cookies) );
	    else if($user_input->selected_control_id === 'setup_item_view_format')
		return ActionFactory::reset_controls( $this->do_get_dialog_item_view_format_defs($plugin_cookies) );

	}
	else if( $user_input->action_type === 'apply' )
	{
	    if( $user_input->control_id === 'setup_store_options' )
	    {
		return $this->dialog_store_options_action($plugin_cookies);
	    }
	    if( $user_input->control_id === 'setup_item_view_format' )
	    {
		return $this->dialog_item_view_format_action($plugin_cookies);
	    }
	    if( $user_input->control_id === 'setup_store_dir_search_deep' )
	    {
		$plugin_cookies->setup_store_dir_search_deep = $user_input->setup_store_dir_search_deep;
		return ActionFactory::reset_controls( $this->do_get_dialog_store_options_defs($plugin_cookies) );
	    }
	    if( $user_input->control_id === 'setup_store_timeout_play' )
	    {
		$plugin_cookies->setup_store_timeout_play = $user_input->setup_store_timeout_play;
		return ActionFactory::reset_controls( $this->do_get_dialog_store_options_defs($plugin_cookies) );
	    }
	}

	return ActionFactory::reset_controls( $this->do_get_control_defs($plugin_cookies) );
    }


    private static function set_default_values(&$plugin_cookies)
    {
	$plugin_cookies->setup_iv_caption_date_fmt = isset($plugin_cookies->setup_iv_caption_date_fmt) ? $plugin_cookies->setup_iv_caption_date_fmt : self::ITEM_VIEW_CAPTION_DATE_FORMAT;

	$plugin_cookies->setup_iv_detail_poster = isset($plugin_cookies->setup_iv_detail_poster) ? $plugin_cookies->setup_iv_detail_poster : self::ITEM_VIEW_DETAIL_POSTER;
	$plugin_cookies->setup_iv_detail_title = isset($plugin_cookies->setup_iv_detail_title) ? $plugin_cookies->setup_iv_detail_title : self::ITEM_VIEW_DETAIL_TITLE;
	$plugin_cookies->setup_iv_detail_start_time = isset($plugin_cookies->setup_iv_detail_start_time) ? $plugin_cookies->setup_iv_detail_start_time : self::ITEM_VIEW_DETAIL_START_TIME;
	$plugin_cookies->setup_iv_detail_duration = isset($plugin_cookies->setup_iv_detail_duration) ? $plugin_cookies->setup_iv_detail_duration : self::ITEM_VIEW_DETAIL_DURATION;
	$plugin_cookies->setup_iv_detail_file_size = isset($plugin_cookies->setup_iv_detail_file_size) ? $plugin_cookies->setup_iv_detail_file_size : self::ITEM_VIEW_DETAIL_FILE_SIZE;
	$plugin_cookies->setup_iv_detail_descr = isset($plugin_cookies->setup_iv_detail_descr) ? $plugin_cookies->setup_iv_detail_descr : self::ITEM_VIEW_DETAIL_DESCRIPTION;

	$plugin_cookies->setup_act_enter = isset( $plugin_cookies->setup_act_enter ) ? $plugin_cookies->setup_act_enter : self::ACTION_FOR_ENTER;
	$plugin_cookies->setup_act_play = isset( $plugin_cookies->setup_act_play ) ? $plugin_cookies->setup_act_play : self::ACTION_FOR_PLAY;
	$plugin_cookies->setup_store_disk = isset( $plugin_cookies->setup_store_disk ) ? $plugin_cookies->setup_store_disk : self::STORE_DISK;
	$plugin_cookies->setup_store_dir = isset( $plugin_cookies->setup_store_dir ) ? $plugin_cookies->setup_store_dir : self::STORE_DIR;
	$plugin_cookies->setup_store_subdir = isset( $plugin_cookies->setup_store_subdir ) ? $plugin_cookies->setup_store_subdir : self::STORE_SUBDIR;
	$plugin_cookies->setup_store_dir_search_deep = isset( $plugin_cookies->setup_store_dir_search_deep ) ?
								$plugin_cookies->setup_store_dir_search_deep : self::SEARCH_STORE_DIR_DEEP;
	$plugin_cookies->setup_store_date_fmt = isset( $plugin_cookies->setup_store_date_fmt ) ? $plugin_cookies->setup_store_date_fmt : self::STORE_DATE_FORMAT;
	$plugin_cookies->setup_store_timeout_play = isset( $plugin_cookies->setup_store_timeout_play ) ? $plugin_cookies->setup_store_timeout_play : self::STORE_TIMEOUT_PLAY;

	$plugin_cookies->setup_check_real_url = isset( $plugin_cookies->setup_check_real_url ) ? $plugin_cookies->setup_check_real_url : self::CHECK_REAL_URL;
    }

    private function dialog_store_options_action(&$plugin_cookies)
    {
	$defs = $this->do_get_dialog_store_options_defs($plugin_cookies);
	return ActionFactory::show_dialog('Store files options', $defs, false, 0);
    }

    private function do_get_dialog_store_options_defs(&$plugin_cookies)
    {
	$defs = array();
	$this->add_label($defs, '', "");
	$this->add_label($defs, 'Current path:', self::get_store_place($plugin_cookies));
	$this->add_label($defs, 'Change:', "");

	$disks = $this->get_disk_list();
	if( count($disks) === 1 )
	    $this->add_label($defs, '    storage:', "check storages are connected");
	else
	    $this->add_combobox($defs, 'setup_store_disk', "    storage:", $plugin_cookies->setup_store_disk, $disks, 0, true);

	$dirs = $this->get_dir_list($plugin_cookies);
	if( count($dirs) === 1 )
	{
	    $msg = $plugin_cookies->setup_store_disk === 'none' ?
			"select storage first" :
			"check storage is connected or create directory first";
	    $this->add_label($defs, "    directory:", $msg);
	}
	else
	{
	    $store_dir_in_list = false;
	    foreach( $dirs as $dir )
		if( $dir === $plugin_cookies->setup_store_dir )
		{
		    $store_dir_in_list = true;
		    break;
		}
	    if( $store_dir_in_list === false )
	    {
		$plugin_cookies->setup_store_dir = 'none';
		return $this->do_get_dialog_store_options_defs($plugin_cookies);
	    }
	    $this->add_combobox($defs, 'setup_store_dir',  "    directory:", $plugin_cookies->setup_store_dir, $dirs, 0, true);
	}

	$this->add_text_field($defs, 'setup_store_dir_search_deep', "Dir search deep:",
				$plugin_cookies->setup_store_dir_search_deep, true, false, false, true, -1, false, true);

	$this->add_label($defs, '', "");

	$this->add_combobox($defs, 'setup_store_subdir', "Store file subdirs:", $plugin_cookies->setup_store_subdir, $this->_opts_store_subdir, 0, true);
	$this->add_combobox($defs, 'setup_store_date_fmt', "Store DateTime format:", $plugin_cookies->setup_store_date_fmt, $this->_opts_store_date_fmt, 0, true);
	$this->add_text_field($defs, 'setup_store_timeout_play', "Play stored file in (seconds):",
				$plugin_cookies->setup_store_timeout_play, true, false, false, true, -1, false, true);

	$this->add_label($defs, '', "");
	$this->add_close_dialog_and_apply_button($defs, 'setup_store_options_save', ' Close ', 0);

	return $defs;
    }



    private function dialog_item_view_format_action(&$plugin_cookies)
    {
	$defs = $this->do_get_dialog_item_view_format_defs($plugin_cookies);
	return ActionFactory::show_dialog('Item view format configure', $defs, false, 0);
    }

    private function do_get_dialog_item_view_format_defs(&$plugin_cookies)
    {
	$defs = array();
	$this->add_label($defs, '', "");

	$this->add_combobox($defs, 'setup_iv_caption_date_fmt', "Item DateTime format:", $plugin_cookies->setup_iv_caption_date_fmt,
				$this->_opts_iv_caption_date_fmt, 0, true);

	$this->add_label($defs, 'Show parameters in detailed info:', '');

	$this->add_combobox($defs, 'setup_iv_detail_poster',	"    Poster:", $plugin_cookies->setup_iv_detail_poster, $this->_opts_yes_no, 0, true);
	$this->add_combobox($defs, 'setup_iv_detail_title', 	"    Title:", $plugin_cookies->setup_iv_detail_title, $this->_opts_yes_no, 0, true);
	$this->add_combobox($defs, 'setup_iv_detail_start_time',"    Start time:", $plugin_cookies->setup_iv_detail_start_time, $this->_opts_yes_no, 0, true);
	$this->add_combobox($defs, 'setup_iv_detail_duration', 	"    Duration:", $plugin_cookies->setup_iv_detail_duration, $this->_opts_yes_no, 0, true);
	$this->add_combobox($defs, 'setup_iv_detail_file_size',	"    File size:", $plugin_cookies->setup_iv_detail_file_size, $this->_opts_yes_no, 0, true);
	$this->add_combobox($defs, 'setup_iv_detail_descr', 	"    Description:", $plugin_cookies->setup_iv_detail_descr, $this->_opts_yes_no, 0, true);

	$this->add_label($defs, '', "");
	$this->add_close_dialog_and_apply_button($defs, 'setup_item_view_format_save', ' Close ', 0);

	return $defs;
    }


    private function _set_combobox_options()
    {
	$this->_opts_yes_no = array();
	$this->_opts_yes_no['yes'] = 'yes';
	$this->_opts_yes_no['no'] = 'no';

	$this->_opts_action = array();
	$this->_opts_action['play'] = 'Play';
	$this->_opts_action['download'] = 'Download';
	$this->_opts_action['download&play'] = 'Download & play';
	$this->_opts_action['info'] = 'Info';


	$this->_opts_store_subdir = array();
	$this->_opts_store_subdir['file'] = 'file';
	$this->_opts_store_subdir['rss/file'] = 'rss/file';
	$this->_opts_store_subdir['service/file'] = 'service/file';
	$this->_opts_store_subdir['service/rss/file'] = 'service/rss/file';

	$this->_opts_store_date_fmt = array();
	$this->_opts_store_date_fmt['ymd_Hi'] = "140815_2208";
	$this->_opts_store_date_fmt['ymd-H:i'] = "140815-22:08";
	$this->_opts_store_date_fmt['Y.m.d-H:i'] = "2014.08.15-22:08";
	$this->_opts_store_date_fmt['Y.m.d-H:i:s'] = "2014.08.15-22:08:00";
	$this->_opts_store_date_fmt['d M H:i'] = "15 Aug 22:08";
	$this->_opts_store_date_fmt['d M Y H:i'] = "15 Aug 2014 22:08";
	$this->_opts_store_date_fmt['none'] = "none";

	$this->_opts_iv_caption_date_fmt = array();
	$this->_opts_iv_caption_date_fmt['d M H:i'] = "28 Aug 20:10";
	$this->_opts_iv_caption_date_fmt['d M Y H:i'] = "28 Aug 2013 20:10";
	$this->_opts_iv_caption_date_fmt['d.m.Y H:i:s'] = "28.08.2013 20:10:00";
	$this->_opts_iv_caption_date_fmt['Y.d.m H:i:s'] = "2013.08.28 20:10:00";
    }




    private function get_disk_list()
    {
	$disks = array();
	$disks['none'] = 'None';
	$fd = dir( self::STORAGE_BASE_DIR );
	if( $fd !== null && $fd !== false )
	{
	    while( false !== ($name = $fd->read()) )
	    {
		if( $name === "." || $name === ".." )
		    continue;
		$disks[$name] = $name;
	    }
	}
	if( count($disks) === 1 )
	    $disks = array();
	return $disks;
    }

    private function get_dir_list($plugin_cookies)
    {
	$disk = $plugin_cookies->setup_store_disk;
	$dirs = array("none");
	if( $disk !== "none" )
	{
	    $this->do_get_dir_list($disk, "", $dirs, 1, $plugin_cookies->setup_store_dir_search_deep);
	}
	return $dirs;
	
    }

    private function do_get_dir_list($disk, $curr_dir, &$dirs, $deep_curr, $deep_max)
    {
	$curr_path = self::STORAGE_BASE_DIR . "/" . $disk . "/" . $curr_dir;
	$fd = dir( $curr_path );
	if( $fd !== null && $fd !== false )
	{
	    while( false !== ($name = $fd->read()) )
	    {
		if( $name === "." || $name === ".." || $name === "lost+found" || !is_dir($curr_path . "/" . $name) )
		    continue;
		if( $curr_dir === "" )
		    $dir = $name;
		else
		    $dir = $curr_dir . "/" . $name;
		$dirs[$dir] = $dir;
		if( $deep_curr < $deep_max )
		    $this->do_get_dir_list($disk, $dir, $dirs, $deep_curr + 1, $deep_max);
	    }
	}
    }
}

?>

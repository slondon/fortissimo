<?php

require_once 'lib/abstract_regular_screen.php';

require_once 'rss_parser.php';
require_once 'service_cfg.php';
require_once 'tools/downloader.php';



class ScreenRss extends AbstractRegularScreen implements UserInputHandler
{
    const ID = 'rss';

    private $_node_storage_map;

    private $_play_file;

    private $_downloader;
    private $_dl_action_after_complete;
    private $_dl_file_info;
    private $_dl_play_phase;


    public function __construct(&$node_storage_map)
    {
        parent::__construct(self::ID, self::get_folder_views());
        UserInputHandlerRegistry::get_instance()->register_handler($this);
        $this->_node_storage_map = $node_storage_map;
        $this->_play_file = NULL;

	$this->_downloader = new Downloader($node_storage_map);
        $this->_dl_action_after_complete = NULL;
        $this->_dl_file_info = NULL;
        $this->_dl_play_phase = 'DL_PLAY_PHASE_NONE';
    }


    public function get_handler_id()			{ return self::ID; }

    public function get_action_map(MediaURL $media_url, &$plugin_cookies)
    {
	$act_enter = ActionFactory::vod_play();
	$act_enter_setup = ScreenSettings::get_act_enter($plugin_cookies);
	if($act_enter_setup === 'download')
	    $act_enter = UserInputHandlerRegistry::create_action($this, 'req_download');
	if($act_enter_setup === 'download&play')
	    $act_enter = UserInputHandlerRegistry::create_action($this, 'req_download&play');
	if($act_enter_setup === 'info')
	    $act_enter = UserInputHandlerRegistry::create_action($this, 'info');

	$act_play = ActionFactory::vod_play();
	$act_play_setup = ScreenSettings::get_act_play($plugin_cookies);
	if($act_play_setup === 'download')
	    $act_play = UserInputHandlerRegistry::create_action($this, 'req_download');
	if($act_play_setup === 'download&play')
	    $act_play = UserInputHandlerRegistry::create_action($this, 'req_download&play');
	if($act_play_setup === 'info')
	    $act_play = UserInputHandlerRegistry::create_action($this, 'info');

	$actions = array
	(
	    GUI_EVENT_KEY_ENTER		=> $act_enter,
	    GUI_EVENT_KEY_PLAY		=> $act_play,
	    GUI_EVENT_KEY_POPUP_MENU	=> UserInputHandlerRegistry::create_action($this, 'popup_menu'),
	    GUI_EVENT_KEY_INFO		=> UserInputHandlerRegistry::create_action($this, 'info'),
	    GUI_EVENT_KEY_C_YELLOW	=> Common::action_changelog($this),
	);

	$act_info_green = Common::action_info_green($this);
	if(!is_null($act_info_green))
	    $actions[GUI_EVENT_KEY_B_GREEN] = $act_info_green;

	return $actions;
    }

    public function handle_user_input(&$user_input, &$plugin_cookies)
    {
	$rc = NULL;
	if (isset($user_input->control_id))
	{
	    $media_url_tmp = MediaUrl::decode($user_input->selected_media_url);
	    $media_url_tmp->__set('screen_id', 'info');
	    $media_url_info = $media_url_tmp->self_encode();

	    $complete_action = NULL;
	    $control_id = $user_input->control_id;
	    switch($control_id)
	    {
		case 'changelog':			return Common::get_changelog_dialog();
		case 'info_green':			return Common::get_info_green_dialog();
		case 'info':				return ActionFactory::open_folder($media_url_info);
		case 'popup_menu':
		{
		    $menu_items[] = array
		    (
			GuiMenuItemDef::caption	=> 'Info',
			GuiMenuItemDef::action	=> ActionFactory::open_folder($media_url_info),
		    );
		    $menu_items[] = array( GuiMenuItemDef::is_separator => true);
		    $menu_items[] = array
		    (
			GuiMenuItemDef::caption	=> 'Download',
			GuiMenuItemDef::action	=> UserInputHandlerRegistry::create_action($this, 'req_download'),
		    );
		    $menu_items[] = array
		    (
			GuiMenuItemDef::caption	=> 'Download & play',
			GuiMenuItemDef::action	=> UserInputHandlerRegistry::create_action($this, 'req_download&play'),
		    );
		    $menu_items[] = array( GuiMenuItemDef::is_separator => true);

		    $parent_url = MediaURL::decode($user_input->parent_media_url);
		    $menu_items[] = array
		    (
			GuiMenuItemDef::caption	=> 'Reload',
			GuiMenuItemDef::action	=> ActionFactory::invalidate_folders(array($parent_url->self_encode())),
		    );
		    return ActionFactory::show_popup_menu($menu_items);
		}
		case 'timer':
		{
		    if($this->_dl_action_after_complete)
		    {
			$actions = array();
			$actions[GUI_EVENT_TIMER] = UserInputHandlerRegistry::create_action($this, 'timer');
			return ActionFactory::change_behaviour($this->get_handler_id(), $actions, $this->_dl_action_after_complete, 2 * 1000);
		    }
		    else
		    {
			if($this->_dl_play_phase === 'DL_PLAY_PHASE_TIMEOUT')
			{
			    $timeout = ScreenSettings::get_store_timeout_play($plugin_cookies);
			    if($timeout > 0)
			    {
				$this->_dl_play_phase = 'DL_PLAY_PHASE_PLAY_FILE';
				$actions = array();
				$actions[GUI_EVENT_TIMER] = UserInputHandlerRegistry::create_action($this, 'timer');
				return ActionFactory::change_behaviour($this->get_handler_id(), $actions, NULL, $timeout * 1000);
			    }
			    else
			    {
				$this->_dl_play_phase = 'DL_PLAY_PHASE_NONE';
				$act_play = UserInputHandlerRegistry::create_action($this, 'do_play_file');
				return ActionFactory::close_dialog_and_run($act_play);
			    }
			}
			else if($this->_dl_play_phase === 'DL_PLAY_PHASE_PLAY_FILE')
			{
			    $this->_dl_play_phase = 'DL_PLAY_PHASE_NONE';
			    $act_play = UserInputHandlerRegistry::create_action($this, 'do_play_file');
			    return ActionFactory::close_dialog_and_run($act_play);
			}
		    }
		    return NULL;
		}
		case 'req_download':
		{
		    $this->_dl_action_after_complete = NULL;
		    $place = ScreenSettings::get_store_place($plugin_cookies);
		    $res_p = $this->_prepare_download($user_input, $plugin_cookies);
		    $file_full = $res_p['file_full'];

		    if(file_exists($file_full) && filesize($file_full))
		    {
			$actions = array();
			$act['control_id'] = 'do_download';
			$act['caption'] = 'download and replace';
			$actions[] = $act;
			$act['control_id'] = 'cancel';
			$act['caption'] = 'cancel';
			$actions[] = $act;
			return $this->_dialog_download_confirm('this file already exists', $res_p['file_name'], $place, $res_p['subdir'],
								filesize($file_full), $actions);
		    }
		    return UserInputHandlerRegistry::create_action($this, 'do_download');
		}
		case 'do_download&play':
		case 'do_download':
		{
		    $res_p = array();
		    $place = ScreenSettings::get_store_place($plugin_cookies);
		    if(!$this->_dl_file_info)
		    {
			$res_p = $this->_prepare_download($user_input, $plugin_cookies);
			$this->_dl_file_info = $res_p;
			$res_d = $this->_downloader->start_download_file($res_p['url'], $res_p['file_path'], $res_p['file_name'], $res_p['dir']);
		    }
		    else
		    {
			$res_p = $this->_dl_file_info;
			$res_d = $this->_downloader->get_state_download_file($res_p['file_full']);
		    }
		    return $this->_dialog_download_do($res_p['url'], $place, $res_p['subdir'], $res_d, $control_id === 'do_download&play');
		}
		case 'req_download&play':
		{
		    $this->_dl_action_after_complete = NULL;
		    $place = ScreenSettings::get_store_place($plugin_cookies);
		    $res_p = $this->_prepare_download($user_input, $plugin_cookies);
		    $file_full = $res_p['file_full'];
		    if(file_exists($file_full) && filesize($file_full))
		    {
			$actions = array();
			$act['control_id'] = 'do_download&play';
			$act['caption'] = 'download and replace';
			$actions[] = $act;
			$act['control_id'] = 'do_play_file';
			$act['caption'] = 'play this file';
			$actions[] = $act;
			$act['control_id'] = 'cancel';
			$act['caption'] = 'cancel';
			$actions[] = $act;
			return $this->_dialog_download_confirm('this file already exists', $res_p['file_name'], $place, $res_p['subdir'],
								filesize($file_full), $actions);
		    }
		    return UserInputHandlerRegistry::create_action($this, 'do_download&play');
		}
		case 'download_cancel':
		    $this->_downloader->cancel_downloading();
		    $this->_dl_action_after_complete = NULL;
		    $this->_dl_file_info = NULL;
		    return NULL;
		case 'do_play_file':
		{
		    $this->_dl_play_phase = 'DL_PLAY_PHASE_NONE';
		    $res_p = $this->_prepare_download($user_input, $plugin_cookies);
		    $this->_play_file = $res_p['file_full'];
		    return ActionFactory::vod_play();
		}
	    }
	}
	return NULL;
    }

    public function get_vod_info4stored_file()
    {
	if($this->_play_file)
	{
	    $file =  basename($this->_play_file);
	    $url = $this->_play_file;
	    $this->_play_file = NULL;
	    return array
	    (
		PluginVodInfo::name		=> $file,
		PluginVodInfo::description	=> $file,
		PluginVodInfo::poster_url	=> 'missing://',
		PluginVodInfo::series	=> array
		(
		    array
		    (
			PluginVodSeriesInfo::name		=> '',
			PluginVodSeriesInfo::playback_url	=> $url,
			PluginVodSeriesInfo::playback_url_is_stream_url => false
		    )
		)
	    );
	}
	return NULL;
    }

    public function update_screen(&$plugin_cookies)
    {
	static $need_update4first = true;

	if(!property_exists($plugin_cookies, 'need_update_screen'))
	    $plugin_cookies->need_update_screen = false;

	if($plugin_cookies->need_update_screen || $need_update4first)
	{
	    $need_update4first = false;
	    $plugin_cookies->need_update_screen = false;
	    $this->folder_views = self::get_folder_views($plugin_cookies);
	}
    }

    public function get_folder_range(MediaURL $media_url, $from_ndx, &$plugin_cookies)
    {
	$ns = $media_url->__get('ns');
	return $this->_node_storage_map[$ns]->get_folder_range($media_url, $from_ndx, $plugin_cookies);
    }

    static function get_folder_views($plugin_cookies = NULL)
    {
	$view_0 = array
	(
	    PluginRegularFolderView::view_params => array
	    (
		ViewParams::num_cols => 1,
		ViewParams::num_rows => 15,
		ViewParams::paint_details => true,
		ViewParams::paint_item_info_in_details => true,
		ViewParams::item_detailed_info_auto_line_break => true,
		ViewParams::animation_enabled => false,
		ViewParams::scroll_animation_enabled => false,
		ViewParams::item_detailed_info_title_color => 3,
	    ),
	    PluginRegularFolderView::base_view_item_params => array
	    (
		ViewItemParams::item_paint_icon => true,
		ViewItemParams::icon_valign => VALIGN_TOP,
		ViewItemParams::item_layout => HALIGN_LEFT,
		ViewItemParams::icon_dx => 15,
		ViewItemParams::icon_sel_dx => 10,
		ViewItemParams::icon_keep_aspect_ratio => true,
		ViewItemParams::icon_scale_factor => 0.5,
		ViewItemParams::icon_sel_scale_factor => 0.6,
		ViewItemParams::item_caption_dx => 40,
		ViewItemParams::item_caption_font_size => FONT_SIZE_SMALL,
	    ),
	    PluginRegularFolderView::not_loaded_view_item_params => array
	    (
		ViewItemParams::item_paint_icon => true,
#		ViewItemParams::item_detailed_icon_path => 'missing://',
		ViewItemParams::item_detailed_icon_path => '',
		ViewItemParams::icon_path => 'gui_skin://osd_icons/wait.aai',
	    ),
	    PluginRegularFolderView::async_icon_loading => true,
#	    PluginRegularFolderView::initial_range => array(),
	);

	if($plugin_cookies && ScreenSettings::need_item_view_detail_poster($plugin_cookies))
	    $view_0[PluginRegularFolderView::view_params][ViewParams::zoom_detailed_icon] = true;

	$view_1 = $view_0;

	$view_1[PluginRegularFolderView::view_params][ViewParams::num_rows] = 10;
	$view_1[PluginRegularFolderView::base_view_item_params][ViewItemParams::item_caption_font_size] = FONT_SIZE_NORMAL;
	$view_1[PluginRegularFolderView::base_view_item_params][ViewItemParams::icon_scale_factor] = 0.8;
	$view_1[PluginRegularFolderView::base_view_item_params][ViewItemParams::icon_sel_scale_factor] = 1.2;
	$view_1[PluginRegularFolderView::base_view_item_params][ViewItemParams::icon_dx] = 20;
	$view_1[PluginRegularFolderView::base_view_item_params][ViewItemParams::icon_sel_dx] = 10;
	$view_1[PluginRegularFolderView::base_view_item_params][ViewItemParams::item_caption_dx] = 70;

	return array( $view_0, $view_1 );
    }


##################################################

    private function _prepare_download($user_input, &$plugin_cookies)
    {
	$media_url = MediaUrl::decode($user_input->selected_media_url);
	$ns = $media_url->__get('ns');
	$item_path = $media_url->__get('path');

	$node = $this->_node_storage_map[$ns]->get_node($item_path, $plugin_cookies);
	$file_url = $node->info['media_url'];
	$item_name = str_replace(array("\r\n", "\n", "\r"), '', $node->name);

	$pub_date = $node->pub_date;
	if(!empty($pub_date))
	{
	    $dt = DateTime::createFromFormat(ScreenSettings::get_item_view_caption_date_format($plugin_cookies), $pub_date);
	    $dt_store_fmt = ScreenSettings::get_store_date_format($plugin_cookies);
	    if($dt_store_fmt !== 'none')
	    {
		$store_date = $dt->format($dt_store_fmt);
		$item_name = "$store_date — $item_name";
	    }
	}

	$res = $this->_downloader->get_store_path($ns, $item_path, $plugin_cookies);
	$path = $res['path'];


	$file = basename($file_url);
	$fileinfo = pathinfo($file);
	$ext = $fileinfo['extension'];
	if($ext)
	{
	    $e = preg_replace('|(.+)\?.*|', '\1', $ext);
	    $file = "$item_name.$e";
	}
	else
	    $file = $item_name;

	$file_full = implode('/', array($path, $file));


	return array
	(
	    'url'	=> $file_url,
	    'file_name'	=> $file,
	    'file_path'	=> $path,
	    'file_full'	=> $file_full,
	    'dir'	=> $res['dir'],
	    'subdir'	=> $res['subdir'],
	);
    }


    private function _dialog_download_confirm($warn, $file, $place, $subdir, $size, $actions)
    {
	$defs = array();
	ControlFactory::add_label($defs, '', '');
	if($warn !== NULL)
	{
	    ControlFactory::add_label($defs, 'WARNING', $warn);
	    ControlFactory::add_label($defs, '', '');
	}
	ControlFactory::add_label($defs, 'file:', $file);
	ControlFactory::add_label($defs, 'place:', $place);
	ControlFactory::add_label($defs, 'dir:', $subdir);
	if($size !== NULL)
	    ControlFactory::add_label($defs, 'size:', get_filesize_str(($size)));
	ControlFactory::add_label($defs, '', '');

	foreach($actions as $a)
	{
	    $action = $a['control_id'] ? UserInputHandlerRegistry::create_action($this, $a['control_id']) : NULL;
	    ControlFactory::add_custom_close_dialog_and_apply_button($defs, $a['caption'], ' ' . $a['caption'] . ' ', 0, $action);
	}

	ControlFactory::add_label($defs, '', '');
	return ActionFactory::show_dialog('Download request', $defs, false, 1600);
    }


    private function _dialog_download_do($url, $place, $subdir, $res_download, $play_after_complete)
    {
	$res = & $res_download;
	$defs = array();

	$status = '';
	if($res['completed'])
	{
	    if(isset($res['error']) && strlen($res['error']))
		$status = 'ERROR: ' . $res['error'];
	    else
		$status = 'COMPLETED SUCCESSFULLY';
	}
	else
	    $status = 'IN PROGRESS';

	$size_str = get_filesize_str(isset($res['size']) ? $res['size'] : 0);
	$percent = 0;
	if(isset($res['percent']))
	{
	    $size_str .= " — " . $res['percent'] . "%";
	    $percent = $res['percent'];
	}


	$pregress_bar_size = 100;
	$num = (int) ($percent / 10);
	$num = (int) $percent;
	$pregress_bar = '|' . str_repeat('.', $num) . str_repeat(' ', $pregress_bar_size - $num) . '|';

	$remain = '00:00:00';
	$elapsed = '00:00:00';
	if(isset($res['remain']))
	    $remain = $res['remain'];
	if(isset($res['elapsed']))
	{
	    $time = new DateTime('@' . $res['elapsed']);
	    $elapsed = $time->format('H:i:s');
	}

	$traf_speed = '';
	if(isset($res['traf_speed']))
	    $traf_speed = $res['traf_speed'];

	ControlFactory::add_label($defs, '', '');
	ControlFactory::add_label($defs, 'status:', $status);
	ControlFactory::add_label($defs, 'url:', $url);
	ControlFactory::add_label($defs, 'file:', $res['file']);
	ControlFactory::add_label($defs, 'place:', $place);
	ControlFactory::add_label($defs, 'dir:', $subdir);
	ControlFactory::add_label($defs, 'size:', $size_str);
	ControlFactory::add_label($defs, '', $pregress_bar);
	ControlFactory::add_label($defs, 'time:', "elapsed: $elapsed, remain: $remain");
	ControlFactory::add_label($defs, 'speed:', $traf_speed);
	ControlFactory::add_label($defs, '', '');

	if(!$res['completed'])
	{
	    $act_cancel = UserInputHandlerRegistry::create_action($this, 'download_cancel');
	    ControlFactory::add_custom_close_dialog_and_apply_button($defs, 'cancel', ' cancel ', 0, $act_cancel);
	}
	else
	{
	    $button_str = ' Ok ';
	    $act_str = 'cancel';
	    if($play_after_complete && !isset($res['error']))
	    {
		$act_str = 'do_play_file';
		$button_str = ' Play ';
		$this->_dl_play_phase = 'DL_PLAY_PHASE_TIMEOUT';
	    }
	    $action = UserInputHandlerRegistry::create_action($this, $act_str);
	    ControlFactory::add_custom_close_dialog_and_apply_button($defs, 'ok', $button_str, 0, $action);
	}

	if(!$this->_dl_action_after_complete)
	{
	    $actions = array();
	    $actions[GUI_EVENT_TIMER] = UserInputHandlerRegistry::create_action($this, 'timer');
	    $this->_dl_action_after_complete = UserInputHandlerRegistry::create_action($this, $play_after_complete ? 'do_download&play' : 'do_download');
	    return ActionFactory::show_dialog('Downloading file', $defs, false, 1600, $actions, $res['completed'] ? 0 : 2 * 1000);
	}

	if($res['completed'])
	{
	    $this->_dl_action_after_complete = NULL;
	    $this->_dl_file_info = NULL;

#	    if($play_after_complete && !isset($res['error']))
	}

	return ActionFactory::reset_controls($defs);
    }


}

?>


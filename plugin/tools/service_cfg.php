<?php


require_once 'exceptions.php';



class ServiceCfg
{
    const SERVICE_CFG_FILENAME = 'service.ini';

    static private $params_cache = array();
    static private $file_mod_time = array();


    static public function load($srv)
    {
	$filename = DuneSystem::$properties['install_dir_path'] . "/services/" . $srv . "/" . self::SERVICE_CFG_FILENAME;
	clearstatcache(true, $filename);
	if( !file_exists($filename) )
	{
	    if( !$file_mod_time[$srv] && isset(self::$params_cache[$srv]) )
	    {
		$params = self::$params_cache[$srv];
	    }
	    else
		throw new FortissimoException("file '" . $filename . "' doesn't exists");
	}
	else
	{
	    $last_mod_time = filemtime($filename);
	    if( $last_mod_time === false )
	    {
		hd_print("check last modification file error: '$filename'");
		self::$file_mod_time[$srv] = 0;
		$params = self::$params_cache[$srv];
	    }
	    else if( isset(self::$file_mod_time[$srv]) &&  $last_mod_time === self::$file_mod_time[$srv] )
	    {
		hd_print("file '$filename' is unchanged");
		$params = self::$params_cache[$srv];
	    }
	    else
	    {
		hd_print("file '$filename' has been modified - loading new config");
		$params = parse_ini_file($filename, true);
		$params['srv_name'] = $srv;
		self::$params_cache[$srv] = $params;
		self::$file_mod_time[$srv] = $last_mod_time;
	    }
	}
	return $params;
    }

}

?>

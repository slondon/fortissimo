<?php

class Tr
{
#    public function __construct()
#    {
#    }


    public static function s($key)
    {
        return "%tr%$key";
    }

    public static function g($key)
    {
        return "%ext%<key_global>$key</key_global>";
    }

    public static function l($key, $values = null)
    {
	$val_str = "";
	if( $values !== null )
	{
	    foreach( $values as $val )
		$val_str .= "<p>" . $val . "</p>";
	}
        return "%ext%<key_local>$key$val_str</key_local>";
    }

#    public static function todo($value)
#    {
#        return $value;
#    }
}

?>

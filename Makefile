


ifndef ver
$(error "need to set ver=x.y.z")
endif

pkt: IN_DIR := plugin
pkt: OUT_FILE := ./updates/dune_plugin_fortissimo-$(ver).tgz
pkt:
	@rm -f $(OUT_FILE)
	@GZIP=-n tar -czf $(OUT_FILE) -C ./$(IN_DIR)/ .
	@md5sum $(OUT_FILE)
	@KB=`du -sk ./$(IN_DIR)/ | awk '{print $$1}'`; echo "$${KB}*1024" | bc


